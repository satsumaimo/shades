#!/bin/bash


if [ "$#" -ne "4" ]
then
    echo $0": give target pdb code, contacts database directory, a file with pdb codes to exclude and directory to pdbs"
    echo "Target must be present in the contact database directory"
    exit 1
fi

PDBCODE=`cat $1 | tr [:upper:] [:lower:]`
DBPATTERN=`cat $2`
EXCLUDE=`cat $3`
DBCLEAN=`cat $4`

contains_not() {
    for j in $2
    do
        if [[ "${j}" == "$1" ]] 
        then
            return 1
        fi
    done
    return 0
}

target=${PDBCODE}"_signature.dat"
TMPF1=`mktemp`
exclusion_list=`cat ${EXCLUDE}`
nbres=`wc -l ${DBPATTERN}/${target} | cut -d' ' -f 1`
ORDERED_REFS=`mktemp`


for ((res=1;res<=${nbres};res++))
do
    head -${res} ${DBPATTERN}/${target} | tail -1 > ${TMPF1}

    lcg=`awk '{print $2}' ${TMPF1}`
    lcd=`awk '{print $3}' ${TMPF1}`
    dc=`awk '{print $4}' ${TMPF1}`
    
    TMPF2=`mktemp`

    lib=`find ${DBPATTERN} -regex '.*\_signature.dat$'`

    for i in ${lib}
    do
        current=`basename ${i} _signature.dat`
        if contains_not "${current}" "${exclusion_list}"
        then
  #          echo "Processing "${current}"..."
            awk -v pdbcode=${current} -v lcg=${lcg} -v lcd=${lcd} -v dc=${dc} -f ./scripts/resolve_query.awk ${i} >> ${TMPF2}
        fi
    done
    mv ${TMPF2} out_${res}.dat
    ./bin/surface_ranker out_${res}.dat ${DBPATTERN} ${PDBCODE} ${res} ${DBCLEAN}
    mv dbfrags.dat ${PDBCODE}_db_${res}.dat
    mv reffrags.dat ${PDBCODE}_ref_${res}.dat
    rm -f out_${res}.dat
    ./scripts/rank_frags.sh ${PDBCODE} ${res}
    awk '{print $2}' ${PDBCODE}_RMS_${res}.dat | sed 's/_/ /g' > ${ORDERED_REFS}
    ./bin/surface_ranker ${ORDERED_REFS} ${DBPATTERN} ${PDBCODE} ${res} lib_${PDBCODE}_${res}.dat ${DBCLEAN}
    echo "Residue "${res}" processed."
done

cat `ls lib_${PDBCODE}_*.dat | sort -t'_' -k 3 -n` > lib_${PDBCODE}.dat
