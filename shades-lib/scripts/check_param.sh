#!/bin/bash

if [ $# -ne "7" ]
then
    echo "usage: ./$0 db dbclean dbpatterns target exclude ccp4 voroprot"
    exit 1
fi

DB=`cat $1`
DBCLEAN=`cat $2`
DBPATTERNS=`cat $3`
TARGET=`cat $4 | tr [:upper:] [:lower:]`
EXCLUDE=`cat $5`
CCP4SET=`cat $6`
VOROPROT=`cat $7`

if [ -d ${DB} ]
then
    echo -e "\033[36mInitial structure database found:\033[0m "${DB}
else
    echo -e "\033[31;1m ERROR: \033[0mInitial structure database does not exist: "${DB}
    exit 1
fi

if [ -d ${DBCLEAN} ]
then
    echo -e "\033[36mCleaned structure database found:\033[0m "${DBCLEAN}
else
    echo -e "\033[33;1m WARNING: \033[0mcleaned structure database does not exist: "${DBCLEAN}
    echo -e "Run \"\033[1mmake pretty\033[0m\" to generate it"
fi

if [ -d ${DBPATTERNS} ]
then
    echo -e "\033[36mPatterns database found: \033[0m"${DBPATTERNS}
else
    echo -e "\033[33;1m WARNING:  \033[0mpatterns database does not exist: "${DBPATTERNS}
    echo -e "Run \"\033[1mmake database\033[0m\" to generate it"
fi

if [ ${TARGET} ]
then
    echo -e "\033[36mTarget is: \033[0m"${TARGET}
    if [ ! -f ${DB}/${TARGET}.pdb ]
    then
        echo -e "\033[31;1m ERROR:\033[0m Please add the target PDB to the initial database \n and run \"\033[1mmake pretty; make database\033[0m\""
        exit 1
    fi
else
    echo -e "\033[31;1m ERROR:\033[0m Target undefined, please give a valid PDB code"
    exit 1
fi

if [ -f ${EXCLUDE} ] && [ ${EXCLUDE} ]
then
    echo -e "\033[36mPDB code exclusion file found:\033[0m "${EXCLUDE}
elif [ ${EXCLUDE} ]
then
    echo -e "\033[36;1mGenerating empty PDB code exclusion file: "${EXCLUDE}
    touch ${EXCLUDE}
else
    echo -e "\033[36;1mGenerating default empty PDB code exclusion file: excluded.dat"
    touch excluded.dat    
fi

if [ -f ${CCP4SET} ] && [ ${CCP4SET} ]
then
    echo -e "\033[36mPDB CCP4 setup file found:\033[0m "${CCP4SET}
else
    echo -e "\033[31;1m ERROR:\033[0m Cannot find CCP4 setup file"
    exit 1
fi

if [ -f ${VOROPROT} ] && [ ${VOROPROT} ]
then
    echo -e "\033[36mVoroprot2_calc.bash found:\033[0m "${VOROPROT}
else
    echo -e "\033[31;1m ERROR:\033[0m Cannot find Voroprot2_calc.bash"
    exit 1
fi
