#!/bin/bash

if [ "$#" -ne "2" ]
then
    echo $0": give a pdb code and an output directory";
    exit 1;
fi

TMP1=`mktemp`
TMP2=`mktemp`
TMP3=`mktemp`

#./scripts/main_chain.sh $1.pdb $1.pdb

./ext/Voroprot2_calc.bash -f $1.pdb -r -a | awk -v f1=$1_map.dat -v f2=${TMP1} -f ./scripts/voro_query.awk
sort -n -k 3 -u $1_map.dat | cut -d' ' -f 3,4 > ${TMP3}
mv ${TMP3} $1_map.dat
awk -v f=${TMP2} -f ./scripts/get_contacts.awk ${TMP1}
awk -v f1=$1_contacts.dat -v f2=$1_signature.dat -f ./scripts/contacts_summary_nodelim.awk ${TMP2}

mv $1_contacts.dat $1_signature.dat $1_map.dat $2
