BEGIN{new_residue=0;printf("") >f1;printf("") >f2;}
$1 ~ /^[[:alpha:]]$/ {
    if (new_residue!=$2)
    {
        if (new_residue)
        {
            print "Residue",new_residue >> f1;
            printf("%d ",new_residue) >> f2;
            printf("%d ", ilcg) >> f1; 
            printf("%d ", ilcg) >> f2; 
            printf("%d ", ilcd) >> f2; 
            printf("%d\n", ild) >> f2; 
            for (i in lcg)
            {
                printf("%d %f ",nlcg[i],lcg[i]) >>f1;
            }
            printf("\n%d ", ilcd) >>f1; 
            for (i in lcd)
            {
                printf("%d %f ",nlcd[i],lcd[i]) >>f1;
            }
            printf("\n%d ", ild) >>f1; 
            for (i in ld)
            {
                printf("%d %f ",nld[i],ld[i]) >>f1;
            }
            printf("\n") >>f1;
        }
        new_residue=$2;
        ilcg=0;
        ilcd=0;
        ild=0;
        delete lcg;
        delete lcd;
        delete ld;
        delete nlcg;
        delete nlcd;
        delete nld;

    }
    if ($4<$2)
    {
        if ($2-$4<5)
        {
            nlcg[ilcg]=$4;
            getline;
            lcg[ilcg]=$2;
            ilcg++;
        }
        else
        {
            nld[ild]=$4;
            getline;
            ld[ild]=$2;
            ild++;
        }
    }
    else if ($4-$2<5)
    {
        nlcd[ilcd]=$4;
        getline;
        lcd[ilcd]=$2;
        ilcd++;
    }
    else
    {
        nld[ild]=$4;
        getline;
        ld[ild]=$2;
        ild++;
    }
}
END{
    print "Residue",new_residue >> f1;
    printf("%d ", ilcg) >> f1; 
    printf("%d ",new_residue) >> f2;
    printf("%d ", ilcg) >> f2; 
    printf("%d ", ilcd) >> f2; 
    printf("%d\n", ild) >> f2; 

    for (i in lcg)
    {
        printf("%d %f ",nlcg[i],lcg[i]) >>f1;
    }
    printf("\n%d ", ilcd) >>f1; 
    for (i in lcd)
    {
        printf("%d %f ",nlcd[i],lcd[i]) >>f1;
    }
    printf("\n%d ", ild) >>f1; 
    for (i in ld)
    {
        printf("%d %f ",nld[i],ld[i]) >>f1;
    }
    printf("\n") >>f1;
}