#!/bin/bash

pdbset xyzin $1 xyzout $2 1>/dev/null <<EOF
select chain A
renumber 1
pick N CA C O
EOF
