#!/bin/bash

if [ "$#" -ne "2" ]
then
    echo "usage: prepare_pdb.sh pdb_database output_dir"
    exit 1
fi

DB=`cat $1`
DBCLEAN=`cat $2`

mkdir -p ${DBCLEAN}
pdb_list=`find ${DB} -regex ".*pdb$"`

PDBTMP=`mktemp`
PDBTMP2=`mktemp`
PDBTMP3=`mktemp`

for i in $pdb_list
do
    target=`basename ${i}`
    awk '$1=="MODEL" && $2!="1" {exit 0;} 
{print $0}' ${i} > ${PDBTMP}.pdb
    ~/bin/main_chain.sh ${PDBTMP}.pdb ${PDBTMP2}.pdb
    awk  '
function three2one(residue)
{
    switch(residue)
    {
        case "ALA": return 1;break;
        case "ARG": return 1;break;
        case "ASN": return 1;break;
        case "ASP": return 1;break;
        case "CYS": return 1;break;
        case "GLU": return 1;break;
        case "GLN": return 1;break;
        case "GLY": return 1;break;
        case "HIS": return 1;break;
        case "ILE": return 1;break;
        case "LEU": return 1;break;
        case "LYS": return 1;break;
        case "MET": return 1;break;
        case "PHE": return 1;break;
        case "PRO": return 1;break;
        case "SER": return 1;break;
        case "THR": return 1;break;
        case "TRP": return 1;break;
        case "TYR": return 1;break;
        case "VAL": return 1;break;
        default: return 0;
    }
}
BEGIN {resnum=-10000; item=0}
$1=="ATOM" { if ((num=strtonum(substr($0,23,4)))==resnum)
{
if (substr($0,17,1)==" " || substr($0,17,1)=="A") 
{ residue = substr($0,18,3); 
if (three2one(residue)==1) { item++; atomrec[item-1]=$0;
}}}
else
{
if (resnum>0 && item==4) { print atomrec[0];
print atomrec[1];
print atomrec[2];
print atomrec[3];  
}
resnum=strtonum(substr($0,23,4));
item=1;
atomrec[0]=$0;
}
}
END {if (resnum>0 && item==4) { print atomrec[0];
print atomrec[1];
print atomrec[2];
print atomrec[3];  
}
}
' ${PDBTMP2}.pdb > ${PDBTMP3}.pdb 
    ~/bin/main_chain.sh ${PDBTMP3}.pdb ${DBCLEAN}/${target}
done
