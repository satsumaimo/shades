#!/bin/bash


if [ "$#" -ne "2" ]
then
    echo ${0}": give an input and output directory"
    exit 1
fi


DBCLEAN=`cat $1`
DBPATTERNS=`cat $2`

mkdir -p ${DBPATTERNS}

find ${DBCLEAN} -regex '.*\.pdb$' > pdbs.txt

list=`cat pdbs.txt`

for i in $list
do
    target=`basename ${i} .pdb`
    ./scripts/compute_contacts.sh ${DBCLEAN}/${target} ${DBPATTERNS}
    echo ${target}" processed."
done
