#!/bin/bash


if [ "$#" -ne "2" ]
then
    echo "Give a pdb code and a residue number"
    exit
fi

list_ref=`ls $1_ref_$2.dat`

RMSFILE=`mktemp`

for i in ${list_ref}
do
    ref=`basename ${i} .dat | cut -d'_' -f 3`
    pdbcode=`basename ${i} .dat | cut -d'_' -f 1`
    db=`echo ${i} | sed "s/ref/db/"`
    nb_res=`grep "ATOM" ${i} | wc -l`
    let nb_res=${nb_res}/4
    echo ${i}
    /home_pers/simoncin/softs/fragger_released/ext/ranker_AA_QCP/ranker_aa ${nb_res} ${db} ${i} 5 > ${RMSFILE}
    sort -n -k1 ${RMSFILE} > ${pdbcode}_RMS_${ref}.dat
done
