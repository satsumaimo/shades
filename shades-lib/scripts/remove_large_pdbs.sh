#!/bin/bash

if [ "$#" -ne "1" ]
then
    echo ${0}": give an input directory"
    exit 1
fi


find $1 -regex '.*\.pdb$' > pdbs.txt

list=`cat pdbs.txt`

for i in $list
do
    target=`basename ${i} .pdb`
    nbres=`egrep "^ATOM" $1/${target}.pdb | grep "CA" | wc -l`
    if (( ${nbres} > 1000 ))
    then
        rm -f $1/${target}.pdb 
    fi
done
