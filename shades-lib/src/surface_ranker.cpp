#include<iostream>
#include<fstream>
#include<sstream>
#include<stdio.h>
#include<stdlib.h>
#include<vector>
#include<string.h>
#include<math.h>
#include<algorithm>

using namespace std;

typedef struct ContactRecord {
  size_t resnum;
  vector < double > surfaces;
  vector < size_t > cresnums;
}ContactRecord;


typedef struct PDBRecord {
  string pdbcode;
  vector < ContactRecord > contacts;
} PDBRecord;
  
typedef struct Fragment {
  string pdbcode;
  size_t resnum;
  vector<size_t> resnums;
  double distance;
} Fragment;

typedef struct ContactInfo {
  size_t resnum;
  string resid;
} ContactInfo;
 
typedef struct FragEntry {
  string resid;
  vector < ContactInfo > local_left;
  vector < ContactInfo > local_right;
  vector < ContactInfo > distant;
} FragEntry;


bool compare_fragments(Fragment frag_one, Fragment frag_two)
{
  return (frag_one.distance<frag_two.distance);
}


vector < Fragment > rankPdbRecords(PDBRecord & ref, vector <PDBRecord> & pdb_recs)
{
  vector < Fragment > fragments;
  for (size_t i=0; i < pdb_recs.size();i++)
    {
      for (size_t j=0; j<pdb_recs[i].contacts.size();j++)
        {
          Fragment fragment;
          fragment.pdbcode = pdb_recs[i].pdbcode;
          if (pdb_recs[i].contacts[j].surfaces.size()!=ref.contacts[0].surfaces.size())
            {
              cout << "Error, number of contacts are different between reference and target" << endl;
              exit(0);
            }
          fragment.resnums.push_back(pdb_recs[i].contacts[j].resnum);
          fragment.resnum = pdb_recs[i].contacts[j].resnum;
          double sum = 0;
          for (size_t k=0; k<pdb_recs[i].contacts[j].surfaces.size();k++)
            {
              sum+=pow(ref.contacts[0].surfaces[k]-pdb_recs[i].contacts[j].surfaces[k],2);
              fragment.resnums.push_back(pdb_recs[i].contacts[j].cresnums[k]);
            }
          fragment.distance = sqrt(sum);
          sort(fragment.resnums.begin(), fragment.resnums.end());
          fragments.push_back(fragment);
        }
    }
  sort(fragments.begin(), fragments.end(), compare_fragments);
  if (fragments.size()>1000)
    fragments.resize(1000);
  return fragments;
} 

PDBRecord get_contacts(string pdbcode, vector < int > resnums, const char * path)
{
  PDBRecord pdb_rec;
  ContactRecord contact;
  pdb_rec.pdbcode = pdbcode;
  vector < ContactRecord > contacts;
  char _fileIn[500];
  sprintf(_fileIn,"%s/%s_contacts.dat",path,pdbcode.c_str());
  ifstream data(_fileIn);
  if (not data.is_open())
    {
      cout << "Couldn't open file " << _fileIn << endl;
      exit(1);
    }
  istringstream line;
  string s;
  bool get_resnum=true;
  bool get_surfaces = false;
  size_t vecsize = resnums.size();
  size_t cur_index=0;
  size_t cur_resnum=0;
  size_t resnum;
  while(!data.eof())
    {
      if (get_resnum && (cur_index<vecsize))
        {
          get_resnum = false;
          cur_resnum = resnums[cur_index];
          cur_index++;
        }
      getline(data,s);
      line.str(s);
      line.clear();
      if (line.str().empty())
        {
          line.str().clear();
          continue;
        }
      else if (line.str().find("Residue")==0)
        {
          // We start a new residue record
          // Is there some data to process ?
          if (get_surfaces)
            {
              pdb_rec.contacts.push_back(contact);
              contact.surfaces.clear();
              contact.cresnums.clear();
            }
          get_surfaces = false;
          char flsh[7];
          // Flushing keyword
          line >> flsh;
          line >> resnum;
          if (resnum == cur_resnum)
            {
              get_resnum = true;
              get_surfaces = true;
            }
          continue;
        }
      if (get_surfaces)
        {
          contact.resnum = resnum;
          size_t nb_contacts;
          size_t c_resnum;
          double contact_surface;
          line >> nb_contacts;
          for (size_t i=0; i< nb_contacts; i++)
            {
              // Get residue number
              line >> c_resnum;
              // Get contact surface
              line >> contact_surface;
              contact.surfaces.push_back(contact_surface);
              contact.cresnums.push_back(c_resnum);
            }
        }
    }
  if (get_surfaces)
    {
      pdb_rec.contacts.push_back(contact);
      contact.surfaces.clear();
      contact.cresnums.clear();
    }
  data.close();
  return pdb_rec;
}


vector < PDBRecord > load_signature_matches(const char * _fileIn, const char * database)
{
  ifstream data(_fileIn);
  if (not data.is_open())
    {
      cout << "Couldn't open file " << _fileIn << endl;
      exit(1);
    }
  string line;
  string pdbcode;
  string resnums_str;
  char * tknizer;
  vector< int > resnums;
  PDBRecord pdb_rec;
  vector < PDBRecord > pdb_recs;
  size_t index;
  while(!data.eof())
    {
      getline(data,line);
      if (!line.empty())
        {
          index=line.find_first_of(" ");
          pdbcode=line.substr(0,index);
          resnums_str=line.substr(index+1,string::npos);
          char* c_resnums_str = new char [resnums_str.length()+1];
          strcpy(c_resnums_str, resnums_str.c_str());
          tknizer = strtok(c_resnums_str," ");
          while(tknizer!=0)
            {
              resnums.push_back(atoi(tknizer));
              tknizer = strtok(NULL," ");
            }
          pdb_rec = get_contacts(pdbcode, resnums, database);
          pdb_recs.push_back(pdb_rec);
          delete[] c_resnums_str;
          resnums.clear();
        }
    }
  data.close();
  return pdb_recs;
}

string map_resnum_to_resid(string pdbcode, size_t resnum, const char * path)
{
  char _fileIn[500];
  string resid;
  sprintf(_fileIn,"%s/%s_map.dat",path,pdbcode.c_str());
  ifstream data(_fileIn);
  if (not data.is_open())
    {
      cout << "Couldn't open file " << _fileIn << endl;
      exit(1);
    }
  istringstream line;
  string s;
  while(!data.eof())
    {
      getline(data,s);
      line.str(s);
      line.clear();
      if (line.str().empty())
        {
          line.str().clear();
        }
      else
        {
          size_t file_resnum;
          line >> file_resnum;
          if (file_resnum==resnum)
            {
              // Skip white spaces
              line >> ws;
              line >> resid;
              break;
            }
        }
    }
  data.close();
  return resid;
}

FragEntry get_fragment_profile(FragEntry & template_frag, Fragment & frag, const char * path)
{
  FragEntry frag_entry;
  ContactInfo contact;
  char _fileIn[500];
  sprintf(_fileIn,"%s/%s_contacts.dat",path,frag.pdbcode.c_str());
  ifstream data(_fileIn);
  if (not data.is_open())
    {
      cout << "Couldn't open file " << _fileIn << endl;
      exit(1);
    }
  istringstream line;
  string s;
  bool get_left = false;
  bool get_right = false;
  bool get_distant = false;
  size_t resnum;
  frag_entry.resid = map_resnum_to_resid(frag.pdbcode, frag.resnum, path);
  while(!data.eof())
    {
      getline(data,s);
      line.str(s);
      line.clear();
      if (line.str().empty())
        {
          line.str().clear();
        }
      else if (line.str().find("Residue")==0)
        {
          char flsh[7];
          // Flushing keyword
          line >> flsh;
          line >> resnum;
          if (resnum == frag.resnum)
            {
              get_left = true;
            }
          continue;
        }
      if (get_left)
        {
          size_t nb_contacts;
          double flush_surface;
          size_t resnum;
          line >> nb_contacts;
          for (size_t i=0; i< nb_contacts; i++)
            {
              // Get residue number
              line >> resnum;
              // Skip contact surface
              line >> flush_surface;
              contact.resnum = template_frag.local_left[i].resnum;
              contact.resid = map_resnum_to_resid(frag.pdbcode, resnum, path);
              frag_entry.local_left.push_back(contact);
            }
          get_left = false;
          get_right = true;
          continue;
        }
      if (get_right)
        {
          size_t nb_contacts;
          double flush_surface;
          size_t resnum;
          line >> nb_contacts;
          for (size_t i=0; i< nb_contacts; i++)
            {
              // Get residue number
              line >> resnum;
              // Skip contact surface
              line >> flush_surface;
              contact.resnum = template_frag.local_right[i].resnum;
              contact.resid = map_resnum_to_resid(frag.pdbcode, resnum, path);
              frag_entry.local_right.push_back(contact);
            }          
          get_right = false;
          get_distant = true;
          continue;
        }
      if (get_distant)
        {
          size_t nb_contacts;
          double flush_surface;
          size_t resnum;
          line >> nb_contacts;
          for (size_t i=0; i< nb_contacts; i++)
            {
              // Get residue number
              line >> resnum;
              // Skip contact surface
              line >> flush_surface;
              contact.resnum = template_frag.distant[i].resnum;
              contact.resid = map_resnum_to_resid(frag.pdbcode, resnum, path);
              frag_entry.distant.push_back(contact);
            }      
          get_distant = false;
          break;
        }
    }
  data.close();
  return frag_entry;
}

FragEntry get_ref_entry(string pdbcode, size_t resnum, const char * path)
{
  FragEntry frag_entry;
  ContactInfo contact;
  char _fileIn[500];
  sprintf(_fileIn,"%s/%s_contacts.dat", path, pdbcode.c_str());
  ifstream data(_fileIn);
  if (not data.is_open())
    {
      cout << "Couldn't open file " << _fileIn << endl;
      exit(1);
    }
  istringstream line;
  string s;
  bool get_left = false;
  bool get_right = false;
  bool get_distant = false;
  size_t file_resnum;
  frag_entry.resid = map_resnum_to_resid(pdbcode, resnum, path);
  while(!data.eof())
    {
      getline(data,s);
      line.str(s);
      line.clear();
      if (line.str().empty())
        {
          line.str().clear();
        }
      else if (line.str().find("Residue")==0)
        {
          char flsh[7];
          // Flushing keyword
          line >> flsh;
          line >> file_resnum;
          if (file_resnum == resnum)
            {
              get_left = true;
            }
          continue;
        }
      if (get_left)
        {
          size_t nb_contacts;
          double flush_surface;
          size_t contact_resnum;
          line >> nb_contacts;
          for (size_t i=0; i< nb_contacts; i++)
            {
              // Get residue number
              line >> contact_resnum;
              // Skip contact surface
              line >> flush_surface;
              contact.resnum = contact_resnum;
              contact.resid = map_resnum_to_resid(pdbcode, contact_resnum, path);
              frag_entry.local_left.push_back(contact);
            }
          get_left = false;
          get_right = true;
          continue;
        }
      if (get_right)
        {
          size_t nb_contacts;
          double flush_surface;
          size_t contact_resnum;
          line >> nb_contacts;
          for (size_t i=0; i< nb_contacts; i++)
            {
              // Get residue number
              line >> contact_resnum;
              // Skip contact surface
              line >> flush_surface;
              contact.resnum = contact_resnum;
              contact.resid = map_resnum_to_resid(pdbcode, contact_resnum, path);
              frag_entry.local_right.push_back(contact);
            }          
          get_right = false;
          get_distant = true;
          continue;
        }
      if (get_distant)
        {
          size_t nb_contacts;
          double flush_surface;
          size_t contact_resnum;
          line >> nb_contacts;
          for (size_t i=0; i< nb_contacts; i++)
            {
              // Get residue number
              line >> contact_resnum;
              // Skip contact surface
              line >> flush_surface;
              contact.resnum = contact_resnum;
              contact.resid = map_resnum_to_resid(pdbcode, contact_resnum, path);
              frag_entry.distant.push_back(contact);
            }      
          get_distant = false;
          break;
        }
    }
  data.close();
  return frag_entry;
}


FragEntry load_ref_descriptor(PDBRecord ref, const char * path)
{
  FragEntry ref_entry;
  if (ref.contacts.size()!=1)
    {
      cout << "Error, there should be only one residue as a reference" << endl;
      exit(0);
    }
  ref_entry = get_ref_entry(ref.pdbcode, ref.contacts[0].resnum, path);
  return ref_entry;
}

bool present(FragEntry & entry, vector < FragEntry> & lib)
{
  if (lib.size()==0)
    return false;
  bool is_there;
  for (size_t i=0; i<lib.size();i++)
    {
      is_there = true;
      if (entry.resid != lib[i].resid)
        is_there = false;
      for (size_t j=0;j<entry.local_left.size();j++)
        {
          if (entry.local_left[j].resid != lib[i].local_left[j].resid)
            is_there = false;
        }
      for (size_t j=0;j<entry.local_right.size();j++)
        {
          if (entry.local_right[j].resid != lib[i].local_right[j].resid)
            is_there = false;
        }
      for (size_t j=0;j<entry.distant.size();j++)
        {
          if (entry.distant[j].resid != lib[i].distant[j].resid)
            is_there = false;
        }
      if (is_there)
        return true;
    }
  return false;
}

vector < FragEntry > load_fragments_descriptor(vector < Fragment > & fragments, PDBRecord ref, const char * path)
{
  vector < FragEntry > fragment_entries;
  FragEntry fragment_entry;
  FragEntry template_frag = load_ref_descriptor(ref, path);
  for (size_t i=0;i<fragments.size();i++)
    {
      fragment_entry = get_fragment_profile(template_frag, fragments[i],path);
      if (!present(fragment_entry, fragment_entries))
        fragment_entries.push_back(fragment_entry);
      if (fragment_entries.size()>=25)
        break;
    }
  return fragment_entries;
}

char three_to_one_letter_code(string resid)
{
  if(resid=="ALA") return 'A';
  else if(resid=="ARG") return 'R';
  else if(resid=="ASN") return 'N';
  else if(resid=="ASP") return 'D';
  else if(resid=="CYS") return 'C';
  else if(resid=="GLU") return 'E';
  else if(resid=="GLN") return 'Q';
  else if(resid=="GLY") return 'G';
  else if(resid=="HIS") return 'H';
  else if(resid=="ILE") return 'I';
  else if(resid=="LEU") return 'L';
  else if(resid=="LYS") return 'K';
  else if(resid=="MET") return 'M';
  else if(resid=="PHE") return 'F';
  else if(resid=="PRO") return 'P';
  else if(resid=="SER") return 'S';
  else if(resid=="THR") return 'T';
  else if(resid=="TRP") return 'W';
  else if(resid=="TYR") return 'Y';
  else if(resid=="VAL") return 'V';
  else { cout << "Unknown residue type: "<< resid << endl;exit(0);}
}


void write_lib(vector < FragEntry > frag_entries, size_t resnum, const char * _fileOut)
{
  ofstream data(_fileOut);
  if (not data.is_open())
    {
      cout << "Couldn't open file " << _fileOut << endl;
      exit(1);
    }
  data << "BEGIN " << resnum << endl;
  for (size_t i=0; i<frag_entries.size(); i++)
    {
      data << frag_entries[i].local_left.size()+frag_entries[i].local_right.size()+frag_entries[i].distant.size();
      data << " " << three_to_one_letter_code(frag_entries[i].resid);
      for (size_t j=0; j<frag_entries[i].local_left.size();j++)
        data << " " << three_to_one_letter_code(frag_entries[i].local_left[j].resid) << " " << frag_entries[i].local_left[j].resnum; 
      for (size_t j=0; j<frag_entries[i].local_right.size();j++)
        data << " " << three_to_one_letter_code(frag_entries[i].local_right[j].resid) << " " << frag_entries[i].local_right[j].resnum; 
      for (size_t j=0; j<frag_entries[i].distant.size();j++)
        data << " " << three_to_one_letter_code(frag_entries[i].distant[j].resid) << " " << frag_entries[i].distant[j].resnum; 
      data << endl;
    }
  data << "END" << endl;
  data.close();
}

vector < FragEntry > load_pdb(string pdbcode, const char * path)
{
  vector < FragEntry > fragment_entries;
  FragEntry entry;
  char _fileIn[500];
  sprintf(_fileIn,"%s/%s_contacts.dat", path, pdbcode.c_str());
  ifstream data(_fileIn);
  if (not data.is_open())
    {
      cout << "Couldn't open file " << _fileIn << endl;
      exit(1);
    }
  string s;
  size_t nb_residues = 0;
  while (!data.eof())
    {
      getline(data,s);
      if (!s.empty())
        if (s.find("Residue")==0)
          nb_residues++;
    }
  for (size_t i=1;i<=nb_residues;i++)
    {
      entry = get_ref_entry(pdbcode, i, path); 
      fragment_entries.push_back(entry);
    }
  return fragment_entries;
}


string construct_uri(string domainName, string pdbcode, size_t resnum)
{
  ostringstream oss;
  oss << "<http://" << domainName << "/" << pdbcode << "/" << resnum << ">";
  return oss.str();

}

void build_prefix_stuff(string domainName, string pdbcode, const char * _fileOut)
{
  ofstream data(_fileOut);
  if (not data.is_open())
    {
      cout << "Couldn't open file " << _fileOut << endl;
      exit(1);
    }
  data << "@prefix protein: <http://" << domainName << "/protein/>." << endl;
  data << "@prefix residue: <http://" << domainName << "/residue/>." << endl;
  data << "@prefix pdbcode: <http://" << domainName << "/" << pdbcode << "/>." << endl;
  // for (size_t i=1;i<=nbres;i++)
  //   {
  //     string uri = construct_uri(domainName, pdbcode, i);
  //     data << "@prefix residue" << i << " "  << uri << endl;
  //   }
  data << endl;
  data.close();
}

void residue_info_to_rdf(FragEntry & entry, size_t resnum, const char * _fileOut)
{
  ofstream data(_fileOut, fstream::app);
  if (not data.is_open())
    {
      cout << "Couldn't open file " << _fileOut << endl;
      exit(1);
    }
  //data << "pdbcode:residue" << resnum << " a " << "protein:residue" << ";" << endl;
  data << "pdbcode:residue" << resnum << " residue:id \"" << entry.resid << "\";" << endl;
  data.close();
}

void contacts_to_rdf(FragEntry & entry, size_t resnum, const char * _fileOut)
{
  ofstream data(_fileOut, fstream::app);
  if (not data.is_open())
    {
      cout << "Couldn't open file " << _fileOut << endl;
      exit(1);
    }
  for (size_t i=0; i<entry.local_left.size();i++)
    {
      if (i)
          data << ";" << endl;
      data << " residue:localleft " << "pdbcode:residue" << entry.local_left[i].resnum; 
    }
  for (size_t i=0; i<entry.local_right.size();i++)
    {
      if (i || entry.local_left.size())
        data << ";" << endl;
      data << " residue:localright " << "pdbcode:residue" << entry.local_right[i].resnum; 
    }
  for (size_t i=0; i<entry.distant.size();i++)
    {
      if (i || entry.local_left.size() || entry.local_right.size())
          data << ";" << endl;
      data << " residue:distant " << "pdbcode:residue" << entry.distant[i].resnum; 
    }
  data << "." << endl;
  data.close();
}

void rdfize_pdb(string domainName, string pdbcode, const char * path)
{
  vector < FragEntry > entries = load_pdb(pdbcode,path);
  build_prefix_stuff(domainName, pdbcode, "test.ttl");
  for (size_t i=0;i<entries.size();i++)
    {
      residue_info_to_rdf(entries[i], i+1, "test.ttl");
      contacts_to_rdf(entries[i], i+1, "test.ttl");
    }
}

void get_residue_from_pdb(ifstream & data_in, size_t resnum, vector< string > & atom_lines)
{
  string s;
  data_in.seekg(0);
  while(!data_in.eof())
    {
      getline(data_in,s);
      istringstream isline(s);
      string token;
      if(s.size())
        {
          isline >> token;
          if (token=="ATOM")
            {
              istringstream is(s.substr(23,4));
              size_t cur_resnum;
              is >> cur_resnum;
              if (cur_resnum>resnum)
                break;
              if (cur_resnum==resnum)
                atom_lines.push_back(s);
            }
        }
    }
}

void write_ref_atom_lines(ContactRecord cr, string pdbcode, string path)
{
  vector < string > atom_lines;
  vector < size_t > resnums;
  resnums.push_back(cr.resnum);
  for (size_t i=0;i<cr.cresnums.size();i++)
    resnums.push_back(cr.cresnums[i]);
  sort(resnums.begin(),resnums.end());
  ofstream data_out("reffrags.dat");
  if (not data_out.is_open())
    {
      cout << "Couldn't open file " << "frags.dat" << endl;
      exit(1);
    }
  string _fileIn=path+"/"+pdbcode+".pdb";
  ostringstream os;
  os << cr.resnum;
  string ref="TER "+pdbcode+"_"+os.str();
  ifstream data_in(_fileIn.c_str());
  if (not data_in.is_open())
        {
          cout << "Couldn't open file " << _fileIn << endl;
          exit(1);
        }
  for(size_t j=0;j<resnums.size();j++)
    get_residue_from_pdb(data_in, resnums[j], atom_lines);
  data_in.close();
  data_out << ref << endl;
  for(size_t i=0;i<atom_lines.size();i++)
    data_out << atom_lines[i] << endl;
  atom_lines.clear();
  //  data_out << "END" << endl;
  data_out.close();
}

void write_atom_lines(vector< Fragment > fragments, string path)
{
  vector < string > atom_lines;
  ofstream data_out("dbfrags.dat");
  if (not data_out.is_open())
    {
      cout << "Couldn't open file " << "frags.dat" << endl;
      exit(1);
    }
  for(size_t i=0;i<fragments.size();i++)
    {
      string _fileIn=path+"/"+fragments[i].pdbcode+".pdb";
      ostringstream os;
      os << fragments[i].resnum;
      string ref="TER "+fragments[i].pdbcode+"_"+os.str();
      ifstream data_in(_fileIn.c_str());
      if (not data_in.is_open())
        {
          cout << "Couldn't open file " << _fileIn << endl;
          exit(1);
        }
      for(size_t j=0;j<fragments[i].resnums.size();j++)
        get_residue_from_pdb(data_in, fragments[i].resnums[j], atom_lines);
      data_in.close();
      data_out << ref << endl;
      for(size_t i=0;i<atom_lines.size();i++)
        data_out << atom_lines[i] << endl;
      atom_lines.clear();
      //      data_out << "END" << endl;
    }
  cout << "Found " << fragments.size() << " fragments" <<  endl;
  data_out.close();
}

int main(int argc, char* argv[])
{
  if (argc!=7 && argc!=6)
    {
      cout << "Wrong number of arguments" << endl;
      exit(0);
    }
  vector < PDBRecord > pdb_recs;
  PDBRecord ref;
  vector < int > ref_resnum;
  vector < Fragment > fragments;
  vector < FragEntry > fragment_entries;
  string pdbrefcode = argv[3];
  ref_resnum.push_back(atoi(argv[4]));
  ref = get_contacts(pdbrefcode, ref_resnum, argv[2]);
  pdb_recs = load_signature_matches(argv[1], argv[2]);
  fragments=rankPdbRecords(ref, pdb_recs);
  if (ref.contacts.size()!=1)
    {
      cout << "Wrong number of contacts in reference fragment" << endl;
      exit(1);
    }
  if (argc==6)
    {
      write_atom_lines(fragments,argv[5]);
      write_ref_atom_lines(ref.contacts[0],ref.pdbcode,argv[5]);
    }
  else
    {
      fragment_entries = load_fragments_descriptor(fragments, ref, argv[2]);
      write_lib(fragment_entries, atoi(argv[4]), argv[5]);
    }
  //rdfize_pdb("www.mypdbrdf","1BGF","/home/simoncin/inputs/full_contacts");
  return EXIT_SUCCESS;
}
