// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
// vi: set ts=2 noet:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @file EdaAbinitio.cc
/// @brief EDA-based ab-initio fragment assembly protocol for proteins
/// @detailed
///	  Contains currently: EdaAbinitio
///
///
/// @author David Simoncini

// Unit Headers
// #include <utility/pointer/boost/owning_ptr.hh>
// #include <utility/pointer/boost/access_ptr.hh>
// #include <utility/pointer/boost/ReferenceCount.hh>

#include <protocols/abinitio/ClassicAbinitio.hh>
#include <protocols/simple_moves/SymmetricFragmentMover.hh>
#include "EdaAbinitio.hh"
#include "EdaPose.hh"
#include "EdaFragmentMover.fwd.hh"
#include "EdaFragmentMover.hh"
#include "EdaTrialMover.hh"
// Package Headers
#include <protocols/simple_moves/GunnCost.hh>

// Project Headers
#include <core/pose/Pose.hh>
#include <core/kinematics/MoveMap.hh>
#include <core/types.hh>
#include <core/scoring/ScoreFunction.fwd.hh>
#include <core/scoring/ScoreType.hh>
#include <core/scoring/ScoreFunctionFactory.hh>
#include <core/scoring/rms_util.hh>
// AUTO-REMOVED #include <core/pack/task/PackerTask.fwd.hh>
#include <core/scoring/constraints/ConstraintSet.hh>
#include <core/scoring/constraints/ConstraintSet.fwd.hh>

#include <protocols/moves/Mover.hh>
#include <protocols/simple_moves/BackboneMover.hh>
#include <protocols/moves/MoverContainer.hh>
#include <protocols/moves/TrialMover.hh>
#include <protocols/moves/RepeatMover.hh>
#include <protocols/moves/WhileMover.hh>
#include <protocols/abinitio/AllResiduesChanged.hh>
//#include <protocols/simple_moves/BackboneMover.hh>

// AUTO-REMOVED #include <protocols/checkpoint/Checkpoint.hh>

// ObjexxFCL Headers
#include <ObjexxFCL/string.functions.hh>

// Utility headers
#include <utility/exit.hh>
#include <utility/vector1.fwd.hh>
#include <utility/pointer/ReferenceCount.hh>
// AUTO-REMOVED #include <utility/file/file_sys_util.hh>
// AUTO-REMOVED #include <utility/io/izstream.hh>
#include <utility/io/ozstream.hh>
#include <numeric/numeric.functions.hh>
#include <basic/prof.hh>
#include <basic/Tracer.hh>
#include <basic/options/option.hh>
#include <basic/options/keys/abinitio.OptionKeys.gen.hh>
#include <basic/options/keys/run.OptionKeys.gen.hh>
#include <basic/options/keys/templates.OptionKeys.gen.hh>

//// C++ headers
#include <cstdlib>
#include <string>
#ifdef WIN32
#include <ctime>
#endif

//debug
// AUTO-REMOVED #include <core/fragment/FragmentIO.hh>

#include <protocols/moves/MonteCarlo.hh>
#include <utility/vector0.hh>
#include <utility/vector1.hh>


#ifdef USEMPI
#include <mpi.h>
#endif

static basic::Tracer tr("protocols.EdaAbinitio");

using core::Real;
using namespace core;
using namespace basic;
using namespace basic::options;
using namespace basic::options::OptionKeys;


class hConvergenceCheck;
typedef  utility::pointer::shared_ptr< hConvergenceCheck >  hConvergenceCheckOP;

class hConvergenceCheck : public protocols::moves::PoseCondition {
public:
	hConvergenceCheck() : bInit_( false ), ct_( 0 ) {}
	void reset() { ct_ = 0; bInit_ = false; }
	void set_trials( protocols::moves::TrialMoverOP trin ) {
		trials_ = trin;
		runtime_assert( trials_->keep_stats_type() < protocols::moves::no_stats );
		last_move_ = 0;
	}
	virtual bool operator() ( const core::pose::Pose & pose );
private:
	core::pose::Pose very_old_pose_;
	bool bInit_;
	Size ct_;
	protocols::moves::TrialMoverOP trials_;
	Size last_move_;
};

// keep going --> return true
bool hConvergenceCheck::operator() ( const core::pose::Pose & pose ) {
	if ( !bInit_ ) {
		bInit_ = true;
		very_old_pose_ = pose;
		return true;
	}
	runtime_assert( trials_ );
	tr.Trace << "TrialCounter in hConvergenceCheck: " << trials_->num_accepts() << std::endl;
	if ( numeric::mod(trials_->num_accepts(),100) != 0 ) return true;
	if ( (Size) trials_->num_accepts() <= last_move_ ) return true;
	last_move_ = trials_->num_accepts();
	// change this later to this: (after we compared with rosetta++ and are happy)
	// if ( numeric::mod(++ct_, 1000) != 0 ) return false; //assumes an approx acceptance rate of 0.1

	// still here? do the check:

	core::Real converge_rms = core::scoring::CA_rmsd( very_old_pose_, pose );
	very_old_pose_ = pose;
	if ( converge_rms >= 3.0 ) {
		return true;
	}
	// if we get here thing is converged stop the While-Loop
	tr.Info << " stop cycles in stage3 due to convergence " << std::endl;
	return false;
}


/*!
@detail call this:
ClassicAbinitio::register_options() before protocols::init::init().
Derived classes that overload this function should also call Parent::register_options()
*/

// This method of adding options with macros is a pain in the ass for people
// trying to nest ClassicAbinitio as part of other protocols. If you don't call
// ClassicAbinitio::register_options() in your main function, you get a really
// unintuitive segfault as the options system doesn't know about the options
// listed below. The solution is to call register_options() in your main method
// before protocols::init::init(), which is really ugly as the main method shouldn't need
// to know what protocols are called, and it's prone to error because it's an
// easy thing to forget.
// This should get some more thought before it becomes the standard way to add options.



//namespace protocols {
//namespace abinitio {

using namespace protocols;
using namespace moves;
//using namespace abinitio;


	EdaAbinitio::EdaAbinitio(
							simple_moves::FragmentMoverOP brute_move_small,
							EdaFragmentMoverOP brute_move_large,
							simple_moves::FragmentMoverOP smooth_move_small,
							int  /*dummy otherwise the two constructors are ambiguous */
													 ):ClassicAbinitio(brute_move_small,brute_move_large,smooth_move_small,1),threshold_(0.1){
		BaseClass::type( "EdaAbinitio" );
		//	set_default_options();
	}
	EdaAbinitio::EdaAbinitio(
							core::fragment::FragSetCOP fragset_small,
							core::fragment::FragSetCOP fragset_large,
							core::kinematics::MoveMapCOP movemap
													 ):ClassicAbinitio(fragset_small,fragset_large,movemap),threshold_(0.1){
		//		simple_moves::ClassicFragmentMoverOP bms, sms;
		BaseClass::type( "EdaAbinitio" );
		//		set_default_options();
	 	EdaFragmentMoverOP bml(new EdaFragmentMover( fragset_large, movemap ));
	// 	bms  = new ClassicFragmentMover( fragset_small, movemap );
	// 	//bms  = new EdaFragmentMover( fragset_large, movemap );
	// 	//sms  = new EdaFragmentMover( fragset_large, movemap );
	// 	sms = new SmoothFragmentMover ( fragset_small, movemap, new GunnCost );
	// 	bms->set_end_bias( option[ OptionKeys::abinitio::end_bias ] ); //default is 30.0
	 	bml->set_end_bias( option[ OptionKeys::abinitio::end_bias ] );
	// 	sms->set_end_bias( option[ OptionKeys::abinitio::end_bias ] );
	 	brute_move_large_= bml;
	 	//set_moves(bms,bml,sms);
	// 	apply_large_frags_   = true;  // apply large frags in phase 2!

	// // in rosetta++ switched on in fold_abinitio if contig_size < 30 in pose_abinitio never
	// 	short_insert_region_ = false;  // apply small fragments in phase 2!

	 	set_trials();
	}
EdaAbinitio::EdaAbinitio( EdaAbinitio const & src ):ClassicAbinitio(src),threshold_(src.threshold_){
	//	brute_move_large_=src.brute_move_large_;
}

// EdaTrialMoverOP EdaAbinitio::trial_large() {
// 	std::cout << "CALLING MY TRIAL LARGE" << std::endl;
// 	return utility::pointer::dynamic_pointer_cast<EdaTrialMover> (trial_large_);
// }

void EdaAbinitio::set_trials() {
	// setup loop1
	Parent::set_trials();
	runtime_assert( brute_move_large_ );
	EdaTrialMoverOP tl_(new EdaTrialMover( brute_move_large_, mc_ptr() ));
	trial_large_ = tl_;
}

double EdaAbinitio::threshold() {
	return threshold_;
}

// void EdaAbinitio::replace_scorefxn( EdaPose& pose, StageID stage, core::Real /*intra_stage_progress */ ) {
// 	// must assume that the current pose is the one to be accepted into the next stage! (this change was necessary for
// 	// checkpointing to work correctly.
	
// 	//intra_stage_progress = intra_stage_progress;
// 	if (score_stage1_  && ( stage == STAGE_1 )) current_scorefxn( *score_stage1_ );
// 	if (score_stage2_  && ( stage == STAGE_2 )) current_scorefxn( *score_stage2_ );
// 	if (score_stage3a_ && ( stage == STAGE_3a)) current_scorefxn( *score_stage3a_ );
// 	if (score_stage3b_ && ( stage == STAGE_3b)) current_scorefxn( *score_stage3b_ );
// 	if (score_stage4_  && ( stage == STAGE_4 )) current_scorefxn( *score_stage4_ );
// 	if (score_stage5_  && ( stage == STAGE_5 )) current_scorefxn( *score_stage5_ );//vats
// 	Real temperature( temperature_ );
// 	if ( stage == STAGE_5 ) temperature = 0.5;
// 	mc_->set_autotemp( true, temperature );
// 	mc_->set_temperature( temperature ); // temperature might have changed due to autotemp..
// 	mc_->reset( pose );
// }


// bool EdaAbinitio::prepare_stage1( core::pose::Pose &pose ) {
// 	replace_scorefxn( pose, STAGE_1, 0.5 );
// 	std::cout << "PREPARING STAGE 1....................................." << std::endl;
// 	mc_ptr()->set_autotemp( false, temperature_ );
// 	//	mc_->set_temperature( temperature_ ); already done in replace_scorefxn
// 	//	mc_->reset( pose );
	
// 	(*score_stage1_)( pose );
// 	/// Now handled automatically.  score_stage1_->accumulate_residue_total_energies( pose ); // fix this
// 	return true;
// }


void EdaAbinitio::apply(core::pose::Pose & pose)
{
	//	Protocol::apply(pose);
	if ( option[ OptionKeys::run::dry_run ]() ) return;

		//basic::prof_reset();
		

	bool success( true );
	total_trials_ = 0;

	if ( !bSkipStage1_ ) {
		PROF_START( basic::STAGE1 );
		clock_t starttime = clock();

		if ( !prepare_stage1( pose ) ) {
			set_last_move_status( moves::FAIL_RETRY );
			return;
		}
		// part 1 ----------------------------------------
		tr.Info <<  "\n===================================================================\n";
		tr.Info <<  "   Stage 1                                                         \n";
		tr.Info <<  "   Folding with score0 for a shitload of fuck " << stage1_cycles() << std::endl;

		if ( option[ basic::options::OptionKeys::run::profile ] ) prof_show();
		if ( option[ basic::options::OptionKeys::abinitio::debug ]() ) {
			output_debug_structure( pose, "stage0" );
		}
		if (!get_checkpoints().recover_checkpoint( pose, get_current_tag(), "stage_1", false /* fullatom*/, true /*fold tree */ )) {
			core::scoring::constraints::ConstraintSetOP orig_constraints(NULL);
			orig_constraints = pose.constraint_set()->clone();
			success = do_stage1_cycles( pose );

 			if ( tr.Info.visible() ) current_scorefxn().show( tr, pose );
			recover_low( pose, STAGE_1 );
			mc().show_counters();
			total_trials_+=mc().total_trials();
			mc().reset_counters();

			pose.constraint_set( orig_constraints ); // restore constraints - this is critical for checkpointing to work
			get_checkpoints().checkpoint( pose, get_current_tag(), "stage_1", true /*fold tree */ );
		} //recover checkpoint
		get_checkpoints().debug( get_current_tag(), "stage_1", current_scorefxn()( pose ) );

		clock_t endtime = clock();
		PROF_STOP( basic::STAGE1 );
		if ( option[ basic::options::OptionKeys::run::profile ] ) prof_show();
		if ( option[ basic::options::OptionKeys::abinitio::debug ]() ) {
			tr.Info << "Timeperstep: " << (double(endtime) - starttime )/(CLOCKS_PER_SEC ) << std::endl;
			output_debug_structure( pose, "stage1" );
		}
	} //skipStage1
	if ( !success ) {
		set_last_move_status( moves::FAIL_RETRY );
		return;
	}


	if ( !bSkipStage2_ ) {

		//
		//
		// part 2 ----------------------------------------
		tr.Info <<  "\n===================================================================\n";
		tr.Info <<  "   Stage 2                                                         \n";
		tr.Info <<  "   Folding with score1 for " << stage2_cycles() << std::endl;

		PROF_START( basic::STAGE2 );
		clock_t starttime = clock();


			if ( close_chbrk_ ){
				Real const setting( 0.25 );
				set_score_weight( scoring::linear_chainbreak, setting, STAGE_2 );
				tr.Info <<  " Chain_break score assigned " << std::endl;
			}


		if ( !prepare_stage2( pose ) )  {
			set_last_move_status( moves::FAIL_RETRY );
			return;
		}

		if (!get_checkpoints().recover_checkpoint( pose, get_current_tag(), "stage_2", false /* fullatom */, true /*fold tree */ )) {
			core::scoring::constraints::ConstraintSetOP orig_constraints(NULL);
			orig_constraints = pose.constraint_set()->clone();

			success = do_stage2_cycles( pose );
			recover_low( pose, STAGE_2 );                    //default OFF: seems to be a bad choice after score0

			if  ( tr.visible() ) current_scorefxn().show( tr, pose );
			mc().show_counters();
			total_trials_+=mc().total_trials();
			mc().reset_counters();

			pose.constraint_set( orig_constraints ); // restore constraints - this is critical for checkpointing to work
			get_checkpoints().checkpoint( pose, get_current_tag(), "stage_2", true /*fold tree */ );
		}
		get_checkpoints().debug( get_current_tag(), "stage_2", current_scorefxn()( pose ) );

		clock_t endtime = clock();
		PROF_STOP( basic::STAGE2 );
		if ( option[ basic::options::OptionKeys::run::profile ] ) prof_show();
		if ( option[ basic::options::OptionKeys::abinitio::debug ]() ) {
			output_debug_structure( pose, "stage2" );
			tr << "Timeperstep: " << (double(endtime) - starttime )/(CLOCKS_PER_SEC ) << std::endl;
		}
	} //bSkipStage2

	if ( !success ) {
		set_last_move_status( moves::FAIL_RETRY );
		return;
	}

	if ( !bSkipStage3_ ) {
		// moved checkpointing into do_stage3_cycles because of structure store

		// part 3 ----------------------------------------
		tr.Info <<  "\n===================================================================\n";
		tr.Info <<  "   Stage 3                                                         \n";
		tr.Info <<  "   Folding with score2 and score5 for " << stage3_cycles() <<std::endl;

		PROF_START( basic::STAGE3 );
		clock_t starttime = clock();

		if ( !prepare_stage3( pose ) ) {
		set_last_move_status( moves::FAIL_RETRY );
			return;
		}
		// this is not the final score-function.. only known after prepare_loop_in_stage3
		// because this is confusing rather not show.if ( tr.Info.visible() ) current_scorefxn().show( tr, pose );

		success = do_stage3_cycles( pose );
		recover_low( pose, STAGE_3b );

		if ( tr.Info.visible() ) current_scorefxn().show( tr, pose );
		mc().show_counters();
		total_trials_+=mc().total_trials();
		mc().reset_counters();

		clock_t endtime = clock();
		PROF_STOP( basic::STAGE3);
		if ( option[ basic::options::OptionKeys::run::profile ] ) prof_show();
		if ( option[ basic::options::OptionKeys::abinitio::debug ]() ) {
			output_debug_structure( pose, "stage3" );
			tr << "Timeperstep: " << (double(endtime) - starttime )/( CLOCKS_PER_SEC) << std::endl;
		}

		//		pose.dump_pdb("stage3.pdb");

	}

	if ( !success ) {
		set_last_move_status( moves::FAIL_RETRY );
		return;
	}
	if ( !bSkipStage4_ ) {

		// part 4 ------------------------------------------
		tr.Info <<  "\n===================================================================\n";
		tr.Info <<  "   Stage 4                                                         \n";
		tr.Info <<  "   Folding with score3 for " << stage4_cycles() <<std::endl;

		PROF_START( basic::STAGE4 );
		clock_t starttime = clock();

		if ( !prepare_stage4( pose ) ) {
			set_last_move_status( moves::FAIL_RETRY );
			return;
		}

		//score-fxn may be changed in do_stage4_cycles...
		// confusing if shown here already... if ( tr.Info.visible() ) current_scorefxn().show( tr, pose);
		success = do_stage4_cycles( pose );
		recover_low( pose, STAGE_4  );

		if ( tr.Info.visible() ) current_scorefxn().show( tr, pose);
		mc().show_counters();
		total_trials_+=mc().total_trials();
		mc().reset_counters();

		clock_t endtime = clock();
		PROF_STOP( basic::STAGE4 );
		if ( option[ basic::options::OptionKeys::run::profile ] ) prof_show();
		if ( option[ basic::options::OptionKeys::abinitio::debug ]() ) {
			output_debug_structure( pose, "stage4" );
			tr << "Timeperstep: " << (double(endtime) - starttime )/( CLOCKS_PER_SEC ) << std::endl;
		}

		tr.Info <<  "\n===================================================================\n";
		tr.Info <<  "   Finished Abinitio                                                 \n";
		tr.Info <<  std::endl;
		//		pose.dump_pdb("stage4.pdb");
	}

	if ( !bSkipStage5_ ) {

		// part 5 ------------------------------------------
		tr.Info <<  "\n===================================================================\n";
		tr.Info <<  "   Stage 5                                                         \n";
		tr.Info <<  "   Folding with score3 for " << stage5_cycles() <<std::endl;

		PROF_START( basic::STAGE5 );
		clock_t starttime = clock();

		if ( !prepare_stage5( pose ) ) {
			set_last_move_status( moves::FAIL_RETRY );
			return;
		}

		success = do_stage5_cycles( pose );
		recover_low( pose, STAGE_5 );

		if ( tr.Info.visible() ) current_scorefxn().show( tr, pose);
		//		current_scorefxn().show(tr, pose);
		mc().show_counters();
		total_trials_+=mc().total_trials();
		mc().reset_counters();

		clock_t endtime = clock();
		PROF_STOP( basic::STAGE5 );
		if ( option[ basic::options::OptionKeys::run::profile ] ) prof_show();
		if ( option[ basic::options::OptionKeys::abinitio::debug ]() ) {
			output_debug_structure( pose, "stage5" );
			tr << "Timeperstep: " << (double(endtime) - starttime )/( CLOCKS_PER_SEC ) << std::endl;
		}

		tr.Info <<  "\n===================================================================\n";
		tr.Info <<  "   Now really finished Abinitio                                                 \n";
		tr.Info <<  std::endl;
		//		pose.dump_pdb("stage5.pdb");

	}

	get_checkpoints().flush_checkpoints();

	if ( !success ) set_last_move_status( moves::FAIL_RETRY );

	basic::prof_show();

	return;

}

// moves::TrialMoverOP
// EdaAbinitio::stage1_mover( EdaPose &, moves::TrialMoverOP trials ) {
// 	return trials;
// }


// moves::TrialMoverOP
// EdaAbinitio::stage2_mover( EdaPose &, moves::TrialMoverOP trials ) {
// 	return trials;
// }


// moves::TrialMoverOP
// EdaAbinitio::stage3_mover( EdaPose &, int, int, moves::TrialMoverOP trials ) {
// 	return trials;
// }

// moves::TrialMoverOP
// EdaAbinitio::stage4_mover( EdaPose &, int, moves::TrialMoverOP trials ) {
// 	return trials;
// }

// moves::TrialMoverOP //vats
// EdaAbinitio::stage5_mover( EdaPose &, moves::TrialMoverOP trials ) {
// 	return trials;
// }


bool EdaAbinitio::do_stage1_cycles( core::pose::Pose &pose ) {
	AllResiduesChanged done( pose, brute_move_large()->insert_map(), *movemap() );
	moves::TrialMoverOP trial( stage1_mover( pose, trial_large() ) );

	//	FragmentMoverOP frag_mover = brute_move_large_;
	//	fragment::FragmentIO().write("stage1_frags_classic.dat",*frag_mover->fragments());
	Size j;
	for ( j = 1; j <= stage1_cycles(); ++j ) {
		trial->apply( pose ); // apply a large fragment insertion, accept with MC boltzmann probability
		if ( done(pose) ) {
			tr.Info << "Replaced extended chain after " << j << " cycles with " << trial->get_name() << std::endl;
			mc().reset(pose ); // make sure that we keep the final structure
			return true;
		}
	}
	tr.Warning << "Warning: extended chain may still remain after " << stage1_cycles() << " cycles!" << std::endl;
	done.show_unmoved( pose, tr.Warning );
	mc().reset( pose ); // make sure that we keep the final structure
	return true;
}

bool EdaAbinitio::do_stage2_cycles( pose::Pose &pose ) {

	//setup cycle
	moves::SequenceMoverOP cycle( new moves::SequenceMover() );
	if ( apply_large_frags_   ) cycle->add_mover( trial_large()->mover() );
	if ( short_insert_region_ ) cycle->add_mover( trial_small()->mover() );

	Size nr_cycles = stage2_cycles() / ( short_insert_region_ ? 2 : 1 );
	moves::TrialMoverOP trials(new moves::TrialMover( cycle, mc_ptr() ));
	moves::RepeatMover( stage2_mover( pose, trials ), nr_cycles ).apply(pose);

	//is there a better way to find out how many steps ? for instance how many calls to scoring?
	return true; // as best guess
}

/*! @detail stage3 cycles:
	nloop1 : outer iterations
	nloop2 : inner iterations
	stage3_cycle : trials per inner iteration
	every inner iteration we switch between score_stage3a ( default: score2 ) and score_stage3b ( default: score 5 )

	prepare_loop_in_stage3() is called before the stage3_cycles() of trials are started.

	first outer loop-iteration is done with TrialMover trial_large()
	all following iterations with trial_small()

	start each iteration with the lowest_score_pose. ( mc->recover_low() -- called in prepare_loop_in_stage3() )

*/
bool EdaAbinitio::do_stage3_cycles( pose::Pose &pose ) {
	using namespace ObjexxFCL;

	// interlaced score2 / score 5 loops
	// nloops1 and nloops2 could become member-variables and thus changeable from the outside
	int nloop1 = 1;
	int nloop2 = 10; //careful: if you change these the number of structures in the structure store changes.. problem with checkpointing
	// individual checkpoints for each stage3 iteration would be a remedy. ...

	if ( short_insert_region_ ) {
		nloop1 = 2;
		nloop2 = 5;
	}

	hConvergenceCheckOP convergence_checker ( NULL );
	if ( !option[ basic::options::OptionKeys::abinitio::skip_convergence_check ] ) {
		hConvergenceCheckOP con_checker(new hConvergenceCheck);
		convergence_checker = con_checker;
	}

	moves::TrialMoverOP trials = trial_large();
	int iteration = 1;
	for ( int lct1 = 1; lct1 <= nloop1; lct1++) {
		if ( lct1 > 1 ) trials = trial_small(); //only with short_insert_region!
		for ( int lct2 = 1; lct2 <= nloop2; lct2++, iteration++  ) {
			tr.Debug << "Loop: " << lct1 << "   " << lct2 << std::endl;

			if ( !prepare_loop_in_stage3( pose, iteration, nloop1*nloop2 ) ) return false;

			if ( !get_checkpoints().recover_checkpoint( pose, get_current_tag(), "stage_3_iter"+string_of( lct1)+"_"+string_of(lct2),
			                                       false /*fullatom */, true /*fold tree */ )) {


				tr.Debug << "  Score stage3 loop iteration " << lct1 << " " << lct2 << std::endl;
				if ( convergence_checker ) {
					moves::TrialMoverOP stage3_trials = stage3_mover( pose, lct1, lct2, trials );
					convergence_checker->set_trials( stage3_trials ); //can be removed late
					moves::WhileMover( stage3_trials, stage3_cycles(), convergence_checker ).apply( pose );
				} else {				//no convergence check -> no WhileMover
					moves::RepeatMover( stage3_mover( pose, lct1, lct2, trials ), stage3_cycles() ).apply( pose );
				}

				if ( numeric::mod( (int)iteration, 2 ) == 0 || iteration > 7 ) recover_low( pose, STAGE_3a );
			                                                                 recover_low( pose, STAGE_3b );

				get_checkpoints().checkpoint( pose, get_current_tag(), "stage_3_iter"+string_of( lct1)+"_"+string_of(lct2), true /*fold tree */ );
			}//recover_checkpoint
			get_checkpoints().debug( get_current_tag(), "stage_3_iter"+string_of( lct1)+"_"+string_of(lct2), current_scorefxn()( pose ) );

			//			structure_store().push_back( mc_->lowest_score_pose() );
		} // loop 2
	} // loop 1
	return true;
}


	// interlaced score2 / score 5 loops
/*! @detail stage4 cycles:
	nloop_stage4: iterations
	stage4_cycle : trials per  iteration

	first iteration: use trial_small()
	following iterations: use trial_smooth()
	only trial_smooth() if just_smooth_cycles==true

	prepare_loop_in_stage4() is called each time before the stage4_cycles_ of trials are started.

	start each iteration with the lowest_score_pose. ( mc->recover_low()  in prepare_loop_in_stage4()  )

*/



void EdaAbinitio::update_probas(utility::vector1< utility::vector1< EdaPose::Item > > & fragkeys_list) {
	utility::pointer::dynamic_pointer_cast<EdaFragmentMover>(brute_move_large_)->update_probas(fragkeys_list);
}

void EdaAbinitio::print_probas(std::ostream & output) {
	utility::pointer::dynamic_pointer_cast<EdaFragmentMover>(brute_move_large_)->print_probas(output);
}

void EdaAbinitio::load_probas(utility::vector1< utility::vector1<double> > & fragKeys) {
	utility::pointer::dynamic_pointer_cast<EdaFragmentMover>(brute_move_large_)->load_probas(fragKeys);
}

void EdaAbinitio::notify_probas() {
	int myrank;
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	utility::vector1< utility::vector1<double> > newKeys( utility::pointer::dynamic_pointer_cast<EdaFragmentMover>(brute_move_large_)->get_frag_keys().size() );
	for (unsigned i=1;i<=newKeys.size();i++)
		newKeys[i].resize(utility::pointer::dynamic_pointer_cast<EdaFragmentMover>(brute_move_large_)->get_frag_keys()[i].size(), (double) 0);
  if (myrank==0)
	  newKeys=(utility::pointer::dynamic_pointer_cast<EdaFragmentMover>(brute_move_large_)->get_frag_keys());
  MPI_Datatype all_vecs;
	MPI_Type_contiguous(newKeys.front().size(), MPI_DOUBLE, &all_vecs);
	//MPI_Type_vector(1, newKeys.size(), newKeys.front().size(),MPI_DOUBLE, &all_vecs);
	MPI_Type_commit(&all_vecs);
	for (unsigned i=1;i<=newKeys.size();i++)
		MPI_Bcast(&newKeys[i].front(),1,all_vecs,0,MPI_COMM_WORLD);
  if (myrank!=0)
		{
			load_probas(newKeys);
		}
	MPI_Type_free(&all_vecs);
}


std::string EdaAbinitio::get_name() const {
	return "EdaAbinitio";
}


	//} //abinitio
	//} //protocols
