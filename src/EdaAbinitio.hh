// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
// vi: set ts=2 noet:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @file src/protocols/abinitio/EdaAbinitio.hh
/// @brief header file for EdaAbinitio protocol
/// @detailed
///	  Contains currently: Eda Abinitio
///
///
/// @author David Simoncini

#ifndef EdaAbinitio_hh
#define EdaAbinitio_hh

// Unit Headers

#include <protocols/simple_moves/FragmentMover.fwd.hh>
#include "EdaFragmentMover.fwd.hh"
#include "EdaPose.fwd.hh"
#include "EdaPose.hh"
#include "EdaTrialMover.fwd.hh"
#include <core/types.hh>

#include <core/kinematics/MoveMap.fwd.hh>
#include <core/pose/Pose.fwd.hh>
#include <core/scoring/ScoreFunction.fwd.hh>
#include <core/scoring/ScoreType.hh>

#include <protocols/abinitio/Protocol.hh>
#include <protocols/abinitio/ClassicAbinitio.fwd.hh>
#include <protocols/abinitio/ClassicAbinitio.hh>
#include <protocols/moves/TrialMover.fwd.hh>
#include "EdaTrialMover.hh"
//#include <utility/pointer/ReferenceCount.hh>
// #include <utility/pointer/boost/owning_ptr.hh>
// #include <utility/pointer/boost/access_ptr.hh>
// #include <utility/pointer/boost/ReferenceCount.hh>

//// C++ headers
#include <string>
#include <vector>

#include <core/fragment/FragSet.fwd.hh>
#include <utility/vector1.hh>


//namespace protocols {
//namespace abinitio {
using namespace protocols;
using namespace abinitio;
/// Move these forward declarations to EdaAbinitio.fwd.hh
class EdaAbinitio;
typedef utility::pointer::shared_ptr< EdaAbinitio > EdaAbinitioOP;


	class EdaAbinitio : public ClassicAbinitio {
	public:
		typedef ClassicAbinitio Parent;
		EdaAbinitio(
								simple_moves::FragmentMoverOP brute_move_small,
								EdaFragmentMoverOP brute_move_large,
								simple_moves::FragmentMoverOP smooth_move_small,
								int  /*dummy otherwise the two constructors are ambiguous */
								);
		EdaAbinitio(
								core::fragment::FragSetCOP fragset_small,
								core::fragment::FragSetCOP fragset_large,
								core::kinematics::MoveMapCOP movemap
								);
		EdaAbinitio( EdaAbinitio const & src );
		std::string get_name() const;

		//EdaTrialMoverOP trial_large();
		
		virtual void set_trials();

		double threshold();

		void apply(core::pose::Pose & pose);
		
		void update_probas(utility::vector1< utility::vector1< EdaPose::Item > > & fragkeys_list);

		void load_probas(utility::vector1< utility::vector1<double> > & fragKeys);

		void print_probas(std::ostream & output);

		void notify_probas();

		bool do_stage1_cycles( core::pose::Pose &pose );
		bool do_stage2_cycles( core::pose::Pose &pose );
		bool do_stage3_cycles( core::pose::Pose &pose );
	private:
		double threshold_;
	};

//} // abinitio
//} // protocols

#endif
