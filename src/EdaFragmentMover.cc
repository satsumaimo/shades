// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
// vi: set ts=2 noet:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @brief EdaFragmentMover
/// @author David Simoncini

// Unit Headers
// #include <utility/pointer/boost/owning_ptr.hh>
// #include <utility/pointer/boost/access_ptr.hh>
// #include <utility/pointer/boost/ReferenceCount.hh>

#include <protocols/simple_moves/FragmentMover.hh>

// Package Headers

// Project Headers
#include <core/fragment/Frame.hh>
#include <core/fragment/FragSet.hh>
#include <core/fragment/ConstantLengthFragSet.hh>
#include <basic/options/option.hh>
#include <core/pose/Pose.hh>
//#include "EdaPose.fwd.hh"
#include "EdaPose.hh"
//#include "EdaFragmentMover.fwd.hh"
#include "EdaFragmentMover.hh"
#include <core/kinematics/FoldTree.hh>

#include <core/kinematics/MoveMap.hh>

// Utility headers
#include <numeric/random/random.hh>
#include <basic/prof.hh>
#include <basic/Tracer.hh>
#include <core/pose/symmetry/util.hh>
// AUTO-REMOVED #include <core/conformation/symmetry/util.hh>

#include <core/conformation/symmetry/SymmetryInfo.hh>

// ObjexxFCL Headers

// option key includes

#include <basic/options/keys/run.OptionKeys.gen.hh>

#include <utility/vector1.hh>
#include <ObjexxFCL/format.hh>




// C++ headers
using namespace protocols;
using	namespace simple_moves;

//static numeric::random::RandomGenerator RG(491);  // <- Magic number, do not change it!

using namespace core;
using namespace fragment;
using namespace basic;

using namespace std;

static basic::Tracer tr("EdaFragmentMover");

///@constructor
EdaFragmentMover::EdaFragmentMover(
	core::fragment::FragSetCOP fragset
																	 )	: ClassicFragmentMover( fragset, "EdaFragmentMover" )
{
	Parent::set_defaults();
	init_frag_keys();
}


///@constructor
EdaFragmentMover::EdaFragmentMover(
	core::fragment::FragSetCOP fragset,
	core::kinematics::MoveMapCOP movemap
)	: ClassicFragmentMover( fragset, movemap, "EdaFragmentMover" )
{
	Parent::set_defaults();
	init_frag_keys();
}


// ///@constructor Temp work around for PyRosetta code, until we found a way how to handle owning pointers in this case
// EdaFragmentMover::EdaFragmentMover(
// 	core::fragment::ConstantLengthFragSet const & fragset,
// 	core::kinematics::MoveMap const & movemap
// )	: ClassicFragmentMover(fragset.clone(), new core::kinematics::MoveMap(movemap), "EdaFragmentMover" )
// {
// 	Parent::set_defaults();
// 	init_frag_keys();
// }

///@brief alternative Constructor to be used by derived classes
EdaFragmentMover::EdaFragmentMover(
	core::fragment::FragSetCOP fragset,
	core::kinematics::MoveMapCOP movemap,
	std::string type
)	: ClassicFragmentMover( fragset, movemap, type )
{
	Parent::set_defaults();
	init_frag_keys();
}

///@brief alternative Constructor to be used by derived classes
EdaFragmentMover::EdaFragmentMover(
	core::fragment::FragSetCOP fragset,
	std::string type
)	: ClassicFragmentMover( fragset, type )
{
	Parent::set_defaults();
	init_frag_keys();
}

EdaFragmentMover::EdaFragmentMover(EdaFragmentMover const & src):ClassicFragmentMover(src) {
	fragKeys=src.fragKeys;
}

std::string EdaFragmentMover::get_name() const {
	return "EdaFragmentMover";
}

void EdaFragmentMover::init_frag_keys() {
	FrameList frames;
	for ( Size i=1; i<=insert_size().size();i++ )
		{
			utility::vector1<double> tmp_vec;
			fragset_->region( *movemap(), i, i,
												min_overlap_, min_frag_length_,	frames );
			Size N ( frames[ 1 ]->nr_frags() );
			double p = (double) 1/N;
			for (Size j=1;j<=N;j++)
				tmp_vec.push_back(p);
			fragKeys.push_back(tmp_vec);
		}
}

void EdaFragmentMover::update_probas(utility::vector1< utility::vector1< EdaPose::Item > > & fragkeys_list) {
	double k=0.6;
	double obs;
	if (fragKeys.size()==0) {
		std::cout << "Empty probas..." << std::endl;
		return;
	}
	utility::vector1< utility::vector1<Size> > fragCounts(fragKeys.size());
	utility::vector1<Size> fragTotals(fragKeys.size(),0);
	for (unsigned i=1;i<=fragKeys.size();i++)
		fragCounts[i].resize(fragKeys[i].size(),0);
	for (unsigned i=1;i<=fragkeys_list.size();i++)
		for (unsigned j=1;j<=fragkeys_list[i].size();j++) {
			fragCounts[fragkeys_list[i][j].frame_pos][fragkeys_list[i][j].frag_num]++;
			fragTotals[fragkeys_list[i][j].frame_pos]++;
		}
	for (unsigned i=1;i<=fragKeys.size();i++)
		if (fragTotals[i]) {
			for (unsigned j=1;j<=fragKeys[i].size();j++) {
				obs = (double) (fragCounts[i][j]) / (double) (fragTotals[i]);
				fragKeys[i][j]=k*fragKeys[i][j]+(1-k)*obs;
			}
		}
}

void EdaFragmentMover::load_probas(utility::vector1< utility::vector1<double> > newFragKeys) {
	fragKeys=newFragKeys;
}


utility::vector1< utility::vector1<double> > & EdaFragmentMover::get_frag_keys() {
	return fragKeys;
}

void EdaFragmentMover::print_probas(std::ostream & output) {
	if (fragKeys.size()==0) {
		output << "Empty probas..." << std::endl;
	}
	for (unsigned i=1;i<=fragKeys.size();i++) {
		for (unsigned j=1;j<=fragKeys[i].size();j++) {
			output << fragKeys[i][j] << " ";
		}
		output << std::endl;
	}
}

//return a fragnum for given Frame, overload to make other choices
bool EdaFragmentMover::choose_fragment(
	FrameList const& frames,
	EdaPose const&,
	Size frag_begin,
	Size& frame_num,
	Size& frag_num
) const {
	// classically: choose randomly
	runtime_assert( frames.size() );
	for ( Size nfail = 1; nfail <= 100; nfail ++ ) {

		//choose frame
		frame_num = static_cast< int >( numeric::random::rg().uniform() * frames.size() ) + 1;
		Size N ( frames[ frame_num ]->nr_frags() );
		//choose frag_num in frame
		if ( N >= 1 ) { // nr_frags is indexed starting at 1
			frag_num = roulette_wheel(fragKeys[frag_begin]);
			return true;
		}
	}
	return false;
}


/// @brief choose and insert a Fragment from the protocols::moves::Movers Fragment-Set into a Pose.

bool EdaFragmentMover::apply_frames( EdaPose &pose, FrameList const& frames, Size frag_begin ) const {
	Size frame_num;
	Size frag_num;
	bool success( false );
	if ( !choose_fragment( frames, pose, frag_begin, frame_num /*output*/, frag_num /*output*/ ) ) return false;
	if ( tr.Trace.visible() ) tr.Trace
															<< "frag (" << frames[ frame_num ]->start() << ","
															<< frag_num << ","
															<< frames[ frame_num ]->nr_res_affected( *movemap_ )
															<< ")" << std::endl;
	if ( !check_ss() ) { 
		success=apply_fragment( *frames[ frame_num ], frag_num, *movemap_, pose );
		if (success)
			pose.set_cur_item(EdaPose::Item(frag_begin,frag_num));
		return success;
	}

	// now do the ss-check!
	//	tr.Trace << "now do the ss-check!"<< std::endl;
	// get actual ss from pose
	std::string proposed_ss;
	proposed_ss.reserve( pose.total_residue() );
	proposed_ss = pose.secstruct();

	std::string old_ss = proposed_ss;

	// check if old ss is valid
	bool valid = !valid_ss( old_ss ); // if old_ss is not valid we can apply the fragment anyway

	// if old ss was fine ---> check fragments effect on ss
	if ( !valid ) { // if old_ss was valid we check if proposed_ss is still valid.
		frames[ frame_num ]->apply_ss( *movemap_, frag_num, proposed_ss );
		//		tr.Trace << !valid << " old_ss: " << old_ss << std::endl;
		valid = valid_ss( proposed_ss );
		//		tr.Trace << valid << "new_ss: " << proposed_ss << std::endl;
	}
	//	tr.Trace << "finished the ss-check! : " << valid << std::endl;
	if ( valid ) {
		success = apply_fragment( *frames[ frame_num ], frag_num, *movemap_, pose );
		pose.set_cur_item(EdaPose::Item(frag_begin,frag_num));
	} else {
		//		tr.Trace << "dissallow insertion due to short helix/strand " << std::endl;
	}
	return success;
}

void EdaFragmentMover::apply( core::pose::Pose & pose ) {
	PROF_START( basic::FRAGMENT_MOVER );
	//	update_insert_map( ); // checks if bValidInsertMap == false

	// If the insert map is empty dont attempt fragment insertions
	// to avoid corrupting the pose, memory and/or your grandmother.
	if( insert_size_.size() == 0 ){
		return;
	}

	// find a fragment
	bool success ( false );

	// since we have an OP of the movemap it might be changed from the outside:
	// this means the insertmap has to be regenerated... do this at most once
	bool insert_map_definitely_right( false );

	Size nfail = 0;
	while ( !success && ( nfail < 100 ) ) {
		Size frag_begin;
		Size window_length;
		FrameList frames;
		while ( nfail < 100 && frames.size() == 0 ) {
			// choose a fragment length
			if ( !choose_window_length( pose, window_length ) ) {
				nfail++;
				continue;
			}


			// choose an insertion point
			//			if( use_predefined_window_start_ )
			//		frag_begin = predefined_window_start_;
			//	else 
			if ( !choose_window_start( pose, window_length, frag_begin ) ) {
				nfail++;
				continue;
			}
			// retrieve fragments for this position
			if ( !fragset_->region( *movemap(), frag_begin, frag_begin + window_length - 1,
					min_overlap_, min_frag_length_,	frames ) ) {
				// if we are here, we couldn't find fragments at this position --- maybe recompute insert_map
				if ( !insert_map_definitely_right ) {
					insert_map_definitely_right = true;
					tr.Debug << "didn't find fragment at predicted position ==> update_insert_map() " << std::endl;
					update_insert_map();
				} else {
					utility_exit_with_message(" couldn't find fragments --- inconsistency in the insert map " );
				}
				nfail++;
				continue;
			} // if fragset_->region
		} // while ( frames.size() == 0 )
			// if ssblock or random_frag only a subset of fragments was there to choose from, this decision could be made
			// outside of this code by supplying a Subset of the frag_set. Alternatively, we could do weight-based sampling
			// like the original design of mini fragments and just set some weights to zero.

		if ( frames.size() ) { // we got fragments
			success = apply_frames( (EdaPose &) pose, frames, frag_begin );
			//poss. reason for failure: ss-check, jump not present in fold-tree
		}
	} // while ( !success );

	if ( !success ) {
		tr.Error << "couldn't find fragment to insert!!" << std::endl;
		return;
	}
	PROF_STOP( basic::FRAGMENT_MOVER );

} // apply


Size EdaFragmentMover::roulette_wheel(utility::vector1<double> vec, double total) const {
	  if (!vec.size())
    {
      cout << "[roulette_wheel] WARNING: NO VECTOR IN INPUT" << endl;
      return 0;
    }
  if (total == 0)
    { // count
      for (unsigned    i = 1; i <= vec.size(); i++)
        total += vec[i];
    }
  double fortune = numeric::random::rg().uniform()*total;
  Size i=0;
  for (i=1;i<vec.size();i++)
    {
      fortune -= vec[i];
      if (fortune <= 0)
        break;
    }
  return i;
}


