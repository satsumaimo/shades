// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
// vi: set ts=2 noet:
// :noTabs=false:tabSize=4:indentSize=4:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @file   EdaFragmentMover.hh
/// @brief  EDA based fragment mover
/// @author David Simoncini
///

#ifndef EdaFragmentMover_HH
#define EdaFragmentMover_HH

// Unit Headers
#include "EdaPose.fwd.hh"
#include "EdaFragmentMover.fwd.hh"
#include <protocols/simple_moves/FragmentMover.fwd.hh>
#include <protocols/simple_moves/FragmentMover.hh>
// Package Headers
// AUTO-REMOVED #include <core/fragment/FragData.fwd.hh>
#include <core/fragment/FragSet.fwd.hh>
#include <core/fragment/Frame.fwd.hh>
#include <core/fragment/FrameList.fwd.hh>
#include <core/fragment/ConstantLengthFragSet.fwd.hh>

// Project Headers
#include <core/kinematics/MoveMap.fwd.hh>
#include <core/pose/Pose.fwd.hh>
#include <protocols/moves/Mover.hh>


// ObjexxFCL Headers

// Utility headers
#include <utility/vector1.fwd.hh>

#include <utility/vector1.hh>


using namespace protocols;
using	namespace simple_moves;

		/// @brief A FragmentMover that applies EDA based sampling of fragments
		class EdaFragmentMover : public ClassicFragmentMover {
		public:

			typedef ClassicFragmentMover Parent;
			/// @brief
			EdaFragmentMover(
											 core::fragment::FragSetCOP fragset
											 );


			/// @brief
			EdaFragmentMover(
											 core::fragment::FragSetCOP fragset,
											 core::kinematics::MoveMapCOP movemap
											 );


			// EdaFragmentMover(
			// 								 core::fragment::ConstantLengthFragSet const & fragset,
			// 								 core::kinematics::MoveMap const & movemap
			// 								 );

			std::string get_name() const;
		protected:

			///@brief alternative Constructor to be used by derived classes
			EdaFragmentMover(
											 core::fragment::FragSetCOP fragset,
											 core::kinematics::MoveMapCOP movemap,
											 std::string type
											 );

			///@brief alternative Constructor to be used by derived classes
			EdaFragmentMover(
											 core::fragment::FragSetCOP fragset,
											 std::string type
											 );

			EdaFragmentMover(EdaFragmentMover const & src);
			
			virtual bool apply_frames( EdaPose & pose, core::fragment::FrameList const&, Size frag_begin ) const;


		public: //this is actually protected: but need public for some unit-testing
			/// @brief apply the chosen fragment,
			/// @detail this can be overloaded to change action, e.,g., WobbleMover introduces chain-break
			/// before fragment insertion and does ccd-loop closure afterwards
			void apply( core::pose::Pose & pose );
			
			void init_frag_keys();
			
			void update_probas(utility::vector1< utility::vector1< EdaPose::Item > > & fragkeys_list);

			void load_probas(utility::vector1< utility::vector1<double> > newFragKeys);

			void print_probas(std::ostream & output);

			utility::vector1< utility::vector1<double> > & get_frag_keys();

			virtual bool choose_fragment(
																	 core::fragment::FrameList const&,
																	 EdaPose const& pose,
																	 Size frag_begin,
																	 Size &frame_num,
																	 Size &frag_num
																	 ) const;
			
			Size roulette_wheel(utility::vector1<double> vec, double total=0) const;
	
		private:
			utility::vector1< utility::vector1<double> > fragKeys; 
			Size min_overlap_;
			Size min_frag_length_;
			bool check_ss_;
			core::Real end_bias_;
			
	// @brief if true, fragments are less likely to be inserted at tails of pose
	// IN JUMPING MODE THIS SHOULD PROBABLY BE MADE RELATIVE TO DIST TO CHAINBREAK ?
			bool bApplyEndBias_;
			bool use_predefined_window_start_;
			Size predefined_window_start_;

		};



#endif
