// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
// vi: set ts=2 noet:
// :noTabs=false:tabSize=4:indentSize=4:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @file   EdaJobDistributor.cc
/// @brief
/// @author David Simoncini

// #include <utility/pointer/boost/owning_ptr.hh>
// #include <utility/pointer/boost/access_ptr.hh>
// #include <utility/pointer/boost/ReferenceCount.hh>

#include <utility/io/izstream.hh>
#include <protocols/jobdist/JobDistributors.hh>
#include "EdaJobDistributor.hh"
#include <basic/Tracer.hh>

#include <core/svn_version.hh>
#include <core/types.hh>
//#include <core/io/pdb/file_data.hh>
#include <core/import_pose/atom_tree_diffs/atom_tree_diff.hh>
#include <core/conformation/Residue.hh>
#include <basic/options/option.hh>
#include <core/pose/Pose.hh>
#include "EdaPose.hh"
#include "EdaAbinitio.hh"
#include <core/scoring/Energies.hh>
#include <core/scoring/ScoreFunction.hh>
// AUTO-REMOVED #include <core/scoring/ScoreFunction.hh>
// AUTO-REMOVED #include <core/scoring/ScoreFunctionFactory.hh>

#include <core/io/silent/util.hh>
#include <core/io/raw_data/DecoyFileData.hh>
#include <core/io/raw_data/ScoreFileData.hh>
#include <core/io/silent/SilentFileData.hh>
#include <core/io/silent/SilentFileData.fwd.hh>
#include <core/io/silent/SilentStructFactory.hh>

#include <numeric/random/random.hh>
#include <numeric/numeric.functions.hh>

#include <utility/exit.hh>
#include <utility/file/file_sys_util.hh>
#include <utility/file/FileName.hh>
#include <utility/io/ozstream.hh>
#include <utility/vector1.hh>
#include <utility/pointer/ReferenceCount.hh>
#ifdef USEMPI
#include <utility/string_util.hh>
#endif

// ObjexxFCL headers
#include <ObjexxFCL/string.functions.hh>

#include <map>
#include <set>
#include <sstream>
#include <string>


// option key includes

#include <basic/options/keys/out.OptionKeys.gen.hh>
#include <basic/options/keys/run.OptionKeys.gen.hh>

#include <protocols/checkpoint/Checkpoint.hh>
#include <numeric/random/random.fwd.hh>


static basic::Tracer tr("EdaJobDistributor");

EdaJobDistributor::EdaJobDistributor(JobVector jobs):
	Parent(jobs)
{
}


EdaJobDistributor::~EdaJobDistributor() {}

void EdaJobDistributor::store_score_and_frags(EdaPose & pose, core::scoring::ScoreFunction const & scorefxn) {
	scores_.push_back(scorefxn(pose));
	fragkeys_list_.push_back(pose.get_fragkeys());
}

// void EdaJobDistributor::dump_fragkeys(
// 																			BasicJobOP const & job,
// 																			EdaPose & pose,
// 																			int const & struct_n
// 																			)
// {
// 	utility::vector1< EdaPose::Item > fragKeys;
// 	std::string output_tag;
// 	this->begin_critical_section();
// 	std::cout << "WRITING KEYS FOR MODEL: " << struct_n << std::endl;
// 	fragKeys = pose.get_fragkeys();
// 	output_tag = this->get_output_tag( job, struct_n );
// 	fragkeys_map_.insert(std::pair< std::string, utility::vector1< EdaPose::Item> > (output_tag, fragKeys) );
// 	std::cout << "SIZE OF THE MAP: " << fragkeys_map_.size() << std::endl;
// 	this->end_critical_section();
// }

void EdaJobDistributor::reload(JobVector jobs) {
	jobs_=jobs;
	current_job_=1;
	scores_.clear();
	fragkeys_list_.clear();
}

void EdaJobDistributor::startup() {

 	// calling base class startup
 	Parent::startup();	
}


#ifdef USEMPI

int EdaJobDistributor::get_mpi_rank() {
	return mpi_rank();
}

void EdaJobDistributor::ship_scores() {
	MPI_Datatype score_vec;
	int vec_size = scores_.size();
	MPI_Type_contiguous(vec_size, MPI_DOUBLE, &score_vec);
	MPI_Type_commit(&score_vec);
	MPI_Send(&vec_size, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
	MPI_Send(&scores_.front(), 1, score_vec, 0, 0, MPI_COMM_WORLD);
	MPI_Type_free(&score_vec);
}

void EdaJobDistributor::receive_scores_and_keys(double rate) {
	utility::vector1< utility::vector1< double > > all_scores;
	unsigned how_many=mpi_nprocs()-1;
	//	utility::vector1< utility::vector1< int > > all_indexes;
	all_scores.resize(how_many);
	for (unsigned i=1;i<=how_many;i++) {
		MPI_Datatype score_vec;
		int vec_size;
		MPI_Recv(&vec_size, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &stat_);
		MPI_Type_contiguous(vec_size, MPI_DOUBLE, &score_vec);
		MPI_Type_commit(&score_vec);
		all_scores[stat_.MPI_SOURCE].resize(vec_size, (double) 0);
		MPI_Recv(&all_scores[stat_.MPI_SOURCE].front(), 1, score_vec, stat_.MPI_SOURCE, 0, MPI_COMM_WORLD, &stat_);
		MPI_Type_free(&score_vec);
		for (unsigned j=1;j<=all_scores[i].size();j++)
			scores_.push_back(all_scores[i][j]);
	}
	// Sort scores, establish threshold. 
	// 1. Collect pose indexes that are below threshold
	// 2. Send vector of pose indexes to each worker
	// 3. Receive fragment keys
	sort(scores_.begin(), scores_.end());
	double threshold_energy = scores_[(int) (scores_.size()*rate)>1?(int) (scores_.size()*rate):1]; // Just a security to prevent crash on dumb threshold value
	for (unsigned i=1; i<=how_many; i++) {
		utility::vector1< int > indexes;
		for (unsigned j=1;j<=all_scores[i].size();j++) {
			if (all_scores[i][j]<=threshold_energy)
				{
					indexes.push_back((int) j);
				}
		}
		int vec_size=indexes.size();
		MPI_Datatype index_vec;
		MPI_Type_contiguous(vec_size, MPI_INT, &index_vec);
		MPI_Type_commit(&index_vec);
		MPI_Send(&vec_size, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
		MPI_Send(&indexes.front(), 1, index_vec, i, 0, MPI_COMM_WORLD);
		MPI_Type_free(&index_vec);
		for (unsigned j=1;j<=indexes.size();j++)
			receive_fragkeys();
		indexes.clear();
	}
}

void EdaJobDistributor::receive_order_and_send_keys() {
	int vec_size;
	MPI_Recv(&vec_size, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &stat_);
	utility::vector1< int > indexes;
	indexes.resize(vec_size);
	MPI_Datatype index_vec;
	MPI_Type_contiguous(vec_size, MPI_INT, &index_vec);
	MPI_Type_commit(&index_vec);
	MPI_Recv(&indexes.front(), 1, index_vec, 0, 0, MPI_COMM_WORLD, &stat_);
	for (unsigned i=1;i<=indexes.size();i++) {
		ship_fragkeys(fragkeys_list_[indexes[i]]);
	}

	MPI_Type_free(&index_vec);
}

void EdaJobDistributor::ship_fragkeys(utility::vector1< EdaPose::Item > fragKeys) {
	// Setting up stuff for communication with master.
	int nb_keys = (int) fragKeys.size();
	MPI_Datatype item, all_items;
	MPI_Datatype types[2];
	MPI_Aint disp[2];
	int block_len[2];
	block_len[0] = block_len[1] = 1;
	types[0] = MPI_INT;
	types[1] = MPI_INT;
	MPI_Address(&fragKeys.front().frame_pos, &disp[0]);
	MPI_Address(&fragKeys.front().frag_num, &disp[1]);
	disp[1] = disp[1]-disp[0];
	disp[0] = 0;
	MPI_Type_struct(2, block_len, disp, types, &item);
	MPI_Type_contiguous(nb_keys, item, &all_items);
	MPI_Type_commit(&all_items);
	MPI_Send(&nb_keys, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
	MPI_Send(&fragKeys.front(), 1, all_items, 0, 0, MPI_COMM_WORLD);
	MPI_Type_free(&all_items);
}

void EdaJobDistributor::receive_fragkeys() {
	// Setting up stuff for communication with master. SHOULD SPECIFY VEC SIZE FOR MASTER....
	utility::vector1< EdaPose::Item > fragKeys;
	int nb_keys;
	MPI_Recv(&nb_keys, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	fragKeys.resize(nb_keys);
	MPI_Datatype item, all_items;
	MPI_Datatype types[2];
	MPI_Aint disp[2];
	int block_len[2];
	block_len[0] = block_len[1] = 1;
	types[0] = MPI_INT;
	types[1] = MPI_INT;
	MPI_Address(&fragKeys.front().frame_pos, &disp[0]);
	MPI_Address(&fragKeys.front().frag_num, &disp[1]);
	disp[1] = disp[1]-disp[0];
	disp[0] = 0;
	MPI_Type_struct(2, block_len, disp, types, &item);
	MPI_Type_contiguous(nb_keys, item, &all_items);
	MPI_Type_commit(&all_items);
	MPI_Recv(&fragKeys.front(), 1, all_items, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	fragkeys_list_.push_back(fragKeys);
	MPI_Type_free(&all_items);
}


#endif


void EdaJobDistributor::estimation_of_distribution(EdaAbinitioOP& prot_ptr) {
#ifdef USEMPI
	if (get_mpi_rank()==0)
		{
			time_t shipping_start_time = time(NULL);
			receive_scores_and_keys(prot_ptr->threshold());
			// for (Size i=0;i<nstruct;i++)
			// 	jobdist.receive_fragkeys();
			time_t shipping_end_time = time(NULL);
			tr.Info << "Shipped data to master in " << shipping_end_time - shipping_start_time << " seconds." << std::endl; 
			prot_ptr->update_probas(fragkeys_list_);
		}
	else {
		ship_scores();
		receive_order_and_send_keys();
	}
	prot_ptr->notify_probas();
	return;
#endif
	
}


int EdaJobDistributor::fragkeys_list_size() {
	return fragkeys_list_.size();
}

void EdaJobDistributor::print_keys(std::ostream & output) { 
	for (unsigned i=1;i<=fragkeys_list_.size();i++)
		{
			for (unsigned j=1;j<=fragkeys_list_[i].size();j++)
				{
					output << fragkeys_list_[i][j].frame_pos << ":" << fragkeys_list_[i][j].frag_num << " ";
				}
			output << std::endl;
		}
}

utility::vector1< utility::vector1< EdaPose::Item > > & EdaJobDistributor::get_fragkeys_list() {
	return fragkeys_list_;
}

