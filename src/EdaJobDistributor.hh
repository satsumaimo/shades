// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
// vi: set ts=2 noet:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @file   protocols/jobdist/JobDistributors.hh
///
/// @brief
/// @author David Simoncini


#ifndef EdaJobDistributor_hh
#define EdaJobDistributor_hh

#define TAG_NUM_FORMAT_LENGTH 8

#ifdef USEMPI
#include <mpi.h>
#endif

// Project headers
// This has to come before boinc.hh or we get this error on VC++
// '_read' : is not a member of 'std::basic_istream<_Elem,_Traits>'
// AUTO-REMOVED #include <utility/io/izstream.hh>

#ifdef BOINC
#include <protocols/boinc/boinc.hh>
#endif // BOINC

#include <protocols/jobdist/JobDistributors.fwd.hh>
#include <protocols/jobdist/JobDistributors.hh>

// AUTO-REMOVED #include <protocols/checkpoint/Checkpoint.hh>

// AUTO-REMOVED #include <core/svn_version.hh>
#include <core/types.hh>
// AUTO-REMOVED #include <core/io/pdb/file_data.hh>
// AUTO-REMOVED #include <core/io/atom_tree_diffs/atom_tree_diff.hh>
// AUTO-REMOVED #include <core/conformation/Residue.hh>
// AUTO-REMOVED #include <basic/options/option.hh>
// AUTO-REMOVED #include <core/pose/Pose.hh>
#include <basic/Tracer.hh>
// AUTO-REMOVED #include <core/scoring/Energies.hh>
// AUTO-REMOVED #include <core/scoring/ScoreFunction.hh>
// AUTO-REMOVED #include <core/scoring/ScoreFunctionFactory.hh>

// AUTO-REMOVED #include <core/io/silent/util.hh>
// AUTO-REMOVED #include <core/io/raw_data/DecoyFileData.hh>
// AUTO-REMOVED #include <core/io/raw_data/ScoreFileData.hh>
// AUTO-REMOVED #include <core/io/silent/SilentFileData.hh>
// AUTO-REMOVED #include <core/io/silent/SilentFileData.fwd.hh>
// AUTO-REMOVED #include <core/io/silent/SilentStructFactory.hh>

// AUTO-REMOVED #include <numeric/random/random.hh>
// AUTO-REMOVED #include <numeric/numeric.functions.hh>

// AUTO-REMOVED #include <utility/exit.hh>
// AUTO-REMOVED #include <utility/file/file_sys_util.hh>
#include <utility/file/FileName.hh>
#include <utility/io/ozstream.hh>
// AUTO-REMOVED #include <utility/vector1.hh>
#include <utility/pointer/ReferenceCount.hh>
// AUTO-REMOVED #include <utility/string_util.hh>

// ObjexxFCL headers
// AUTO-REMOVED #include <ObjexxFCL/string.functions.hh>

#include <map>
#include <set>
#include <sstream>
#include <string>


// option key includes

// AUTO-REMOVED #include <basic/options/keys/out.OptionKeys.gen.hh>
// AUTO-REMOVED #include <basic/options/keys/run.OptionKeys.gen.hh>

#include <core/io/silent/silent.fwd.hh>
#include <core/pose/Pose.fwd.hh>
#include "EdaPose.fwd.hh"
#include "EdaAbinitio.fwd.hh"
#include "EdaPose.hh"
#include <utility/vector1.hh>



#ifdef BOINC
#ifdef USEMPI
Throw a compiler error because MPI and BOINC cannot be used together!
If you got this message, something is wrong with your build settings.
#endif
#endif


using namespace protocols::jobdist;

class EdaJobDistributor : public PlainSilentFileJobDistributor
{
public:
  typedef PlainSilentFileJobDistributor Parent;

protected:
	typedef utility::vector1< BasicJobOP > JobVector;

public:

	EdaJobDistributor(JobVector jobs);

	virtual ~EdaJobDistributor();

	///@brief Writes pose and basic score data to a standard silent file, stores fragment keys as well.
	// virtual void dump_fragkeys(
	// 													 BasicJobOP const & job,
	// 													 EdaPose & pose,
	// 													 int const & nstruct
	// 													 );

	void reload(JobVector jobs);

	virtual void startup();
	
	void store_score(EdaPose & pose, core::scoring::ScoreFunction const & scorefxn);

	void ship_scores();
	
	#ifdef USEMPI

	void receive_scores_and_keys(double rate);

	void receive_order_and_send_keys();

	void ship_fragkeys(utility::vector1< EdaPose::Item > fragKeys);

	void store_score_and_frags(EdaPose & pose, core::scoring::ScoreFunction const & scorefxn);

	void receive_fragkeys();

	int get_mpi_rank();
	
	#endif

	void estimation_of_distribution(EdaAbinitioOP& prot_ptr);

	int fragkeys_list_size();

	utility::vector1< utility::vector1< EdaPose::Item > > & get_fragkeys_list();

	void print_keys(std::ostream & output);


protected:

	//utility::vector1< std::string > used_tags_;
	utility::vector1< utility::vector1< EdaPose::Item > > fragkeys_list_;
	utility::vector1< double > scores_;
};


#endif
