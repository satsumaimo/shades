
// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
// vi: set ts=2 noet:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @file   EdaPose.cc
/// @brief  EdaPose class
/// @author David Simoncini

// Unit headers
#include "EdaPose.hh"
#include <core/pose/Pose.hh>

// package headers
#include <core/pose/PDBInfo.hh>
#include <core/pose/datacache/CacheableDataType.hh>
#include <core/pose/datacache/CacheableObserverType.hh>
#include <core/pose/datacache/ObserverCache.hh>

// Project headers
#include <core/chemical/ResidueType.hh>
#include <core/conformation/Residue.hh>
#include <core/conformation/Conformation.hh>

#include <core/id/TorsionID.hh>
#include <core/scoring/Energies.hh>
// AUTO-REMOVED #include <core/scoring/ScoreFunctionInfo.hh>
#include <core/scoring/ScoreFunction.hh>
//#include <core/io/pdb/file_data.hh>
#include <core/scoring/constraints/ConstraintSet.hh>
#include <basic/datacache/BasicDataCache.hh>
// AUTO-REMOVED #include <basic/MetricValue.hh>
#include <basic/prof.hh>
#include <core/pose/metrics/PoseMetricContainer.hh>
// AUTO-REMOVED #include <core/io/pdb/pose_io.hh>

#include <core/chemical/AA.hh>
// AUTO-REMOVED #include <core/chemical/ResidueTypeSet.hh>

#include <basic/options/option.hh>
#include <basic/options/keys/in.OptionKeys.gen.hh>

#include <utility/vector1.hh>

#include <algorithm>
#include <iostream>
#include <fstream>
//#include <core/pack/dunbrack/RotamerLibrary.fwd.hh>
// AUTO-REMOVED #include <core/scoring/mm/MMBondAngleResidueTypeParamSet.fwd.hh>
//#include <core/optimization/MinimizerMap.fwd.hh>
#include <basic/options/keys/OptionKeys.hh>

//Auto Headers
//#include <core/pack/rotamer_set/RotamerSet.fwd.hh>
//#include <core/pack/task/PackerTask.fwd.hh>
#include <core/pose/util.hh>
#include <core/pose/signals/ConformationEvent.hh>
#include <core/pose/signals/DestructionEvent.hh>
#include <core/pose/signals/EnergyEvent.hh>
//#include <core/pack/dunbrack/RotamerLibrary.fwd.hh>

#include <utility/vector0.hh>
#include <numeric/xyz.functions.hh>

#include <core/id/NamedAtomID.hh>

//Auto Headers
#include <core/conformation/signals/XYZEvent.hh>
#include <core/kinematics/AtomTree.hh>
#include <core/kinematics/FoldTree.hh>
#include <core/kinematics/tree/Atom.hh>




using namespace core;
using namespace pose;

EdaPose::EdaPose():Pose(),cur_item(EdaPose::Item(-1,-1))
{
}

EdaPose::EdaPose(EdaPose const & src):Pose(src),cur_item(EdaPose::Item(-1,-1))
{
	fragKeys=src.fragKeys;
}
EdaPose::EdaPose(EdaPose const & src, Size begin, Size const end):Pose(src),cur_item(EdaPose::Item(-1,-1))
{
	fragKeys.clear();
	for(; begin <= end; ++begin){
		fragKeys.push_back(src.fragKeys[begin]);
	}
}
EdaPose & EdaPose::operator=(EdaPose const & src)
{
	static_cast<Parent&>(*this)=Parent::operator=(src);
	fragKeys=src.fragKeys;
        cur_item=src.cur_item;
	return *this;
}
void EdaPose::update_frags(EdaPose::Item frag)
{
  fragKeys[frag.frame_pos]=frag;
	//	int pos;
	// pos=window_present(frag.frame_pos);
	// if (pos)
	// 	fragKeys[pos]=frag;
	// else
	// 	fragKeys.push_back(frag);
	//	pushback_frags(frag);
}

void EdaPose::pushback_frags(EdaPose::Item frag)
{
	fragKeys.push_back(frag);
}

int EdaPose::window_present(int frame_pos)
{
	for (unsigned i=1;i<=fragKeys.size();i++)
		{
			if (fragKeys[i].frame_pos==frame_pos)
				return i;
		}
	return 0;
}	


EdaPose::Item EdaPose::get_cur_item() {
	return cur_item;
}

void EdaPose::set_cur_item(EdaPose::Item item) {
	cur_item = item;
}

int EdaPose::get_fragkeys_size() {
	return fragKeys.size();
}

void EdaPose::resize_frag_keys() {
  fragKeys.resize(total_residue()-8);
}

utility::vector1< EdaPose::Item > EdaPose::get_fragkeys() {
	return fragKeys;
}
