// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
//  vi: set ts=2 noet:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @file   EdaPose.fwd.hh
/// @brief  EdaPose forward declarations header
/// @author David Simoncini

#ifndef EdaPose_fwd_hh
#define EdaPose_fwd_hh

// #include <utility/pointer/boost/owning_ptr.hh>
// #include <utility/pointer/boost/access_ptr.hh>
// #include <utility/pointer/boost/ReferenceCount.hh>


//#include <utility/pointer/access_ptr.hh>
//#include <utility/pointer/owning_ptr.hh>
#include <utility/vector1.hh>

// Forward
class EdaPose;

typedef utility::pointer::shared_ptr< EdaPose > EdaPoseOP;
typedef utility::pointer::shared_ptr< EdaPose const > EdaPoseCOP;

typedef utility::pointer::weak_ptr< EdaPose > EdaPoseAP;
typedef utility::pointer::weak_ptr< EdaPose const > EdaPoseCAP;

typedef utility::vector1< EdaPoseOP > EdaPoseOPs;
typedef utility::vector1< EdaPoseCOP > EdaPoseCOPs;



#endif
