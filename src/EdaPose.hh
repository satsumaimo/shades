// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
// vi: set ts=2 noet:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @file   EdaPose.hh
/// @brief  EdaPose class
/// @author David Simoncini


#ifndef EdaPose_hh
#define EdaPose_hh


// type headers
#include <core/types.hh>

// #include <utility/pointer/boost/owning_ptr.hh>
// #include <utility/pointer/boost/access_ptr.hh>
// #include <utility/pointer/boost/ReferenceCount.hh>

// Unit headers
#include <core/pose/Pose.hh>
//#include "EdaPose.fwd.hh"

// Package headers
#include <core/pose/PDBInfo.fwd.hh>
#include <core/pose/datacache/ObserverCache.fwd.hh>
#include <core/pose/metrics/PoseMetricContainer.fwd.hh>

// Project headers
#include <core/chemical/ResidueType.fwd.hh>
#include <core/chemical/AA.hh>

#include <core/conformation/Residue.fwd.hh>
#include <core/conformation/Conformation.fwd.hh>
#include <core/conformation/signals/XYZEvent.fwd.hh>

// AUTO-REMOVED #include <core/id/AtomID.hh>
// AUTO-REMOVED #include <core/id/DOF_ID.hh>
// AUTO-REMOVED #include <core/id/NamedAtomID.hh>
// AUTO-REMOVED #include <core/id/NamedStubID.hh>
#include <core/id/TorsionID.fwd.hh>

#include <core/kinematics/AtomTree.fwd.hh>
#include <core/kinematics/FoldTree.fwd.hh>
#include <core/kinematics/Jump.fwd.hh>
#include <core/kinematics/Stub.fwd.hh>

#include <core/scoring/Energies.fwd.hh>
#include <core/scoring/ScoreFunction.fwd.hh>
// AUTO-REMOVED #include <core/scoring/ScoreFunctionInfo.fwd.hh>
#include <core/scoring/constraints/Constraint.fwd.hh>
// AUTO-REMOVED #include <core/scoring/constraints/Constraints.fwd.hh>
#include <core/scoring/constraints/ConstraintSet.fwd.hh>

#include <basic/datacache/BasicDataCache.fwd.hh>
#include <basic/MetricValue.fwd.hh>

// //#include "cst_set.h"
// #include "jump_classes.h"
// #include "pose_param.h"
// #include "score_data.h"

// // ObjexxFCL Headers
// #include <ObjexxFCL/FArray3A.hh>
// #include <ObjexxFCL/FArray4D.hh>

// Utility headers
// #include <utility/pointer/access_ptr.hh>
// #include <utility/pointer/owning_ptr.hh>
// #include <utility/pointer/boost/access_ptr.hh>
// #include <utility/pointer/boost/owning_ptr.hh>


#include <utility/signals/BufferedSignalHub.hh>
#include <utility/vector1.fwd.hh>

// Numeric headers
#include <numeric/xyzMatrix.fwd.hh>
#include <numeric/xyzVector.fwd.hh>

#include <core/id/AtomID.fwd.hh>
#include <core/id/DOF_ID.fwd.hh>
#include <core/id/NamedAtomID.fwd.hh>
#include <core/id/NamedStubID.fwd.hh>
#include <core/pose/signals/ConformationEvent.fwd.hh>
#include <core/pose/signals/DestructionEvent.fwd.hh>
#include <core/pose/signals/EnergyEvent.fwd.hh>
#include <core/pose/signals/GeneralEvent.fwd.hh>
#include <utility/vector1.hh>


#ifdef WIN32
	#include <core/pose/signals/ConformationEvent.hh>
	#include <core/pose/signals/DestructionEvent.hh>
	#include <core/pose/signals/EnergyEvent.hh>
	#include <core/pose/signals/GeneralEvent.hh>
	#include <core/conformation/Residue.hh>
	#include <core/pose/datacache/CacheableObserver.hh>
	#include <core/id/AtomID.hh>
#endif


// C++ Headers
// #include <cassert>
// #include <cmath>
// #include <cstdlib>
// #include <iostream>
// #include <map>
// #include <string>


using namespace core;
using namespace pose;

//class EdaPose;

class EdaPose: public Pose {
public:
  typedef Pose Parent;

  struct Item {
    Item( int a, int b) : frame_pos( a), frag_num( b) {}
		Item() :  frame_pos( 1), frag_num( 1) {}
    int frame_pos;
    int frag_num;
  };

  EdaPose();
  EdaPose(EdaPose const & src);
  EdaPose( EdaPose const & src, Size residue_begin, Size residue_end);
  EdaPose & operator=(EdaPose const & src);
  //  void log_frag();
  void update_frags(Item frag);
	void pushback_frags(Item frag);
	int window_present(int frame_pos);
  Item get_cur_item();
  void set_cur_item(Item item);
  int get_fragkeys_size();
	void resize_frag_keys();
	utility::vector1< Item > get_fragkeys();
private:
  utility::vector1< Item > fragKeys;
  Item cur_item;
};


#endif
