// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
// vi: set ts=2 noet:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @file EdaTrialMover
/// @brief performs a move and accepts it according to Monte Carlo accept/reject criterion and stores accepted fragments in EdaPose.
/// @author David Simoncini



// Rosetta Headers
#include "EdaPose.hh"
#include "EdaTrialMover.hh"
#include <core/pose/Pose.hh>
// AUTO-REMOVED #include <core/conformation/Residue.hh>
#include <core/scoring/Energies.hh>
#include <core/scoring/ScoreFunction.hh>
// AUTO-REMOVED #include <basic/basic.hh>
#include <basic/Tracer.hh>
#include <utility/tag/Tag.hh>

#include <protocols/moves/MonteCarlo.hh>
#include <protocols/moves/TrialMover.hh>
#include <protocols/moves/Mover.hh>
//#include <basic/datacache/DataMap.hh>
// Utility headers

// C++ headers
#include <string>

#include <protocols/jobdist/Jobs.hh>
#include <utility/vector0.hh>
#include <utility/vector1.hh>


/// @begin EdaTrialMover::apply()
/// @brief:
/// 	the apply function for a trial
///	@detailed:
///		the trial object is created with an mc object
///		the mover is applied before doing an mc.boltzmann
///   Accepted moves are stored in EdaPose
///	@author: David Simoncini

using namespace protocols;
using namespace moves;

EdaTrialMover::EdaTrialMover():Parent() {}

EdaTrialMover::EdaTrialMover( MoverOP mover_in, MonteCarloOP mc_in ):Parent(mover_in, mc_in) {}


void EdaTrialMover::apply( pose::Pose & pose )
{	
	using scoring::total_score;
	/// get the initial scores
	if ( keep_stats_type() == all_stats ) {
		stats_.add_score( mc_->last_accepted_score() ); ///< initial_last_accepted_score
		stats_.add_score( pose.energies().total_energy() ); ///< initial_pose_score
	}

	/// make the move
	mover_->apply( pose );

	// if ( keep_stats_type() == all_stats ) { //// score and get residue energies
	// Stupid and wasteful.  The structure will be scored inside mc_->boltzman.  mc_->score_function()( pose );
	// Unneccessary since revision 23846 --- mc_->score_function().accumulate_residue_total_energies( pose );
	// WAIT FOR IT. stats_.add_score( pose.energies().total_energy() ); ///< score_after_move
	// }
	/// test if MC accepts or rejects it
	bool accepted_move = mc_->boltzmann( pose, mover_->type() );
	if (accepted_move) {
		((EdaPose &) pose).update_frags(((EdaPose &) pose).get_cur_item());
	}

	if ( keep_stats_type() == all_stats ) {
		stats_.add_score( mc_->total_score_of_last_considered_pose() );
	}

	if ( stats_type_ <= accept_reject ) {
		stats_.accepted( accepted_move );
	}
	if ( keep_stats_type() > no_stats ) {
		stats_.print( mc_, mover_->type() );
	}
}

std::string
EdaTrialMover::get_name() const {
	return "EdaTrialMover";
}
