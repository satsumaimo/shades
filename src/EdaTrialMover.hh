// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
// vi: set ts=2 noet:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @file EdaTrialMover
/// @brief performs a move and accepts it according to Monte Carlo accept/reject criterion.
/// @author David Simoncini


#ifndef EdaTrialMover_hh
#define EdaTrialMover_hh

// Unit headers
//#include <protocols/moves/TrialMover.fwd.hh>
#include "EdaTrialMover.fwd.hh"

// Package headers
#include <protocols/moves/TrialMover.hh>
#include <protocols/moves/Mover.hh>
#include <protocols/moves/MonteCarlo.fwd.hh>
#include <protocols/moves/MoverStatistics.hh>

#include <core/scoring/ScoreType.hh>

#include <core/types.hh>

#include <core/pose/Pose.fwd.hh>

// ObjexxFCL Headers

// C++ Headers
#include <map>
#include <string>

#include <utility/vector1.hh>


using namespace protocols;
using namespace moves;


class EdaTrialMover : public TrialMover {
public:
	typedef core::Real Real;
	typedef TrialMover Parent;
	EdaTrialMover();
	EdaTrialMover( MoverOP mover_in, MonteCarloOP mc_in );

public:

	virtual void apply( core::pose::Pose & pose );
	virtual std::string get_name() const;

private:
		StatsType stats_type_;

}; 


#endif 
