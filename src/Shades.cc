#include "Shades.hh"
#include "ShadesLoader.hh"
#include "ShadesMover.hh"
#include <iostream>
#include <sstream>
#include <string>
#include <core/import_pose/import_pose.hh>
#include <core/pose/Pose.hh>
#include <core/pose/util.hh>
#include <basic/options/option.hh>
#include <basic/options/keys/in.OptionKeys.gen.hh>
#include <basic/options/keys/out.OptionKeys.gen.hh>
#include <basic/options/keys/abinitio.OptionKeys.gen.hh>
#include <basic/options/keys/shades.OptionKeys.gen.hh>
#include <basic/options/keys/backrub.OptionKeys.gen.hh>
#include <basic/options/keys/constraints.OptionKeys.gen.hh>
#include <basic/options/keys/run.OptionKeys.gen.hh>
#include <basic/options/keys/relax.OptionKeys.gen.hh>

#include <utility/pointer/access_ptr.hh>
#include <utility/pointer/owning_ptr.hh>

#include <protocols/jobdist/JobDistributors.hh>
#include <protocols/jobdist/Jobs.hh>
#include <protocols/relax/ClassicRelax.hh>
#include <protocols/relax/FastRelax.hh>
#include <protocols/relax/util.hh>
#include <protocols/abinitio/ResolutionSwitcher.hh>
#include<core/import_pose/import_pose.hh>
#include <core/scoring/ScoreFunction.fwd.hh>
#include <core/scoring/ScoreType.hh>
#include <core/scoring/ScoreFunctionFactory.hh>
#include <core/scoring/func/Func.hh>
#include <core/scoring/constraints/BoundConstraint.hh>
#include <core/scoring/MembraneTopology.hh>
#include <core/scoring/methods/ContactOrderEnergy.hh>
#include <core/pack/pack_rotamers.hh>
#include <core/pack/task/TaskFactory.hh>
#include <core/pack/task/PackerTask.hh>
//#include <core/pack/task/operation/TaskOperations.hh>

#include <core/kinematics/MoveMap.hh>

#include <core/optimization/AtomTreeMinimizer.hh>
#include <core/optimization/MinimizerOptions.hh>
#include <core/scoring/constraints/util.hh>
#include <core/scoring/constraints/ConstraintIO.hh>
#include <core/scoring/constraints/ConstraintSet.hh>
#include <core/scoring/constraints/ConstraintSet.fwd.hh>
#include <core/scoring/constraints/AtomPairConstraint.hh>
#include <core/scoring/func/HarmonicFunc.hh>
#include <core/id/AtomID.hh>
#include <core/id/DOF_ID.hh>
#include <core/kinematics/Jump.hh>


#include <protocols/idealize/IdealizeMover.hh>
#include <protocols/moves/MonteCarlo.hh>
#include <protocols/backrub/BackrubProtocol.hh>
#include <protocols/evaluation/PoseEvaluator.hh>
#include <protocols/evaluation/EvaluatorFactory.hh>
#include <protocols/evaluation/PCA.hh>

#include <core/io/silent/silent.fwd.hh>
#include <core/io/silent/SilentStructFactory.hh>

#include <basic/Tracer.hh>

#include "ShadesJobDistributor.hh"

using namespace std;
using namespace basic::options;
using namespace basic::options::OptionKeys;
using namespace core;
using namespace protocols;
using namespace evaluation;
using namespace core::scoring;
using namespace core::scoring::constraints;
using namespace id;


using namespace ObjexxFCL;

using basic::T;
using basic::Error;
using basic::Warning;

using namespace protocols::moves;

using utility::vector1;

static basic::Tracer tr("Shades");

class Minimize : public moves::Mover {

public:
	Minimize();

	~Minimize() override;

	MoverOP clone() const override;
	MoverOP fresh_instance() const override;

	void apply( Pose & pose ) override;
	std::string get_name() const override;
	void test_move( Pose & pose ) override
	{
		apply(pose);
	}

private:
	ScoreFunctionOP score_function_;
};

Minimize::Minimize() :
	Mover( "benchmark" ),
	score_function_( get_score_function() )
{

	using namespace basic::options;
	using namespace basic::options::OptionKeys;

	if ( option[ in::file::fullatom ]() ) {
		core::scoring::constraints::add_fa_constraints_from_cmdline_to_scorefxn( *score_function_ );
	} else {
		core::scoring::constraints::add_constraints_from_cmdline_to_scorefxn( *score_function_ );
	}


}

Minimize::~Minimize() = default;

MoverOP Minimize::clone() const {
	return MoverOP( new Minimize( *this ) );
}
MoverOP Minimize::fresh_instance() const {
	return MoverOP( new Minimize );
}

void
Minimize::apply( Pose & pose ) {
  using namespace pose;
  using namespace basic::options;
  using namespace basic::options::OptionKeys;


  if ( option[ in::file::fullatom ]() ) {
    core::scoring::constraints::add_fa_constraints_from_cmdline_to_pose( pose );
  } else {
    core::scoring::constraints::add_constraints_from_cmdline_to_pose( pose );
  }


  std::string min_type =  option[ OptionKeys::run::min_type ]();
  core::Real min_tol =  option[ OptionKeys::run::min_tolerance ]();
  core::optimization::MinimizerOptions options( min_type, min_tol, true, false );
  core::kinematics::MoveMap final_mm;

  if ( !option[ OptionKeys::in::file::movemap ].user() ) {
    final_mm.set_chi( option[ OptionKeys::relax::chi_move ].user() ? option[ OptionKeys::relax::chi_move ]() : true  );
    final_mm.set_bb( option[ OptionKeys::relax::bb_move ].user() ? option[ OptionKeys::relax::bb_move ]() : true  );
    final_mm.set_jump( option[ OptionKeys::relax::jump_move ].user() ? option[ OptionKeys::relax::jump_move ]() : true );
  } else {
    tr << "Initializing movemap from file " << option[ OptionKeys::in::file::movemap ]() << std::endl;
    final_mm.init_from_file(option[ OptionKeys::in::file::movemap ]() );
  }

  /*core::Real start_score =*/ (*score_function_)(pose);
  core::Size repeats = 1;
  for ( core::Size i = 0; i < repeats; i++ ) {
    core::optimization::AtomTreeMinimizer().run( pose, final_mm, *score_function_, options );
    tr << "Score: " << i << "  " <<  (*score_function_)(pose) << std::endl;
  }

  core::Real final_score = (*score_function_)(pose);
  tr << "FinalScore: " << final_score << std::endl;

}

std::string
Minimize::get_name() const {
	return "Minimize";
}


//using namespace core::pack::task::operation;




// ScoreFunction&
// reduce_fa_rep(float fraction_fa_rep, ScoreFunction & s){
// 	s.set_weight( core::scoring::score_type_from_name("fa_rep"),
// 		s.get_weight(core::scoring::score_type_from_name("fa_rep"))*fraction_fa_rep);
// 	return s;
// }


// void
// minimize_shades(pose::Pose & p, ScoreFunction & s){
//   using namespace basic::options;
//   //silent file capabilities
//   core::optimization::AtomTreeMinimizer min_struc;
//   float minimizer_tol = 0.000001;
  
//   core::kinematics::MoveMap mm;
//   mm.set_bb(true);
//   mm.set_chi(true);
//   s.show(std::cout, p);
//   if ( basic::options::option[basic::options::OptionKeys::constraints::cst_file].user() ) {
//     core::scoring::constraints::ConstraintSetOP cstset( new core::scoring::constraints::ConstraintSet() );
//     cstset = core::scoring::constraints::ConstraintIO::read_constraints(option[basic::options::OptionKeys::constraints::cst_file][1],cstset,p);
//     p.constraint_set(cstset);
//   }
//   // This used to be higher, but then it couldn't respond to the adjustment
//   // for in case we are only doing sidechain minimization!
//   core::optimization::MinimizerOptions options( "lbfgs_armijo_nonmonotone", minimizer_tol, true /*use_nb_list*/,
//                                                 false /*deriv_check_in*/, true /*deriv_check_verbose_in*/);
  
//   options.nblist_auto_update( true );
//   options.max_iter(5000); //otherwise, they don't seem to converge
  

//   //set scorefxn fa_rep to 1/10 of original weight and then minimize
//   ScoreFunctionOP one_tenth_orig(s.clone());
//   reduce_fa_rep(0.1,*one_tenth_orig);
//   //min_struc.run(p,mm,s,options);
//   min_struc.run(p,mm,*one_tenth_orig,options);
//   std::cout << "one tenth repulsive fa_rep score-function" << std::endl;
//   one_tenth_orig->show(std::cout, p);
  
//   //then set scorefxn fa_rep to 1/3 of original weight and then minimize
//   ScoreFunctionOP one_third_orig(s.clone());
//   reduce_fa_rep(0.33,*one_third_orig);
//   min_struc.run(p,mm,*one_third_orig,options);
//   std::cout << "one third repulsive fa_rep score-function" << std::endl;
//   one_third_orig->show(std::cout, p);
//   //then set scorefxn fa_rep to original weight and then minimize
  
//   pose::Pose before(p);
  
//   min_struc.run(p,mm,s,options);
                
//     //repack
//   std::cout << "repacking" << std::endl;
//   ScoreFunctionOP spack = ScoreFunctionFactory::create_score_function("soft_rep_design.wts");
//   pack::task::PackerTaskOP repack(pack::task::TaskFactory::create_packer_task(p));
//   repack->restrict_to_repacking();
//   for ( unsigned int i = 1; i <= p.size(); i++ ) {
//     repack->nonconst_residue_task(i).or_include_current(true);
//     repack->nonconst_residue_task(i).or_ex1(true);
//     repack->nonconst_residue_task(i).or_ex2(true);
//   }
//   pack::pack_rotamers(p,(*spack),repack);

  
//   s.show(std::cout, p);
//   while ( std::abs(s(p)-s(before)) > 1 ) { //make sure furhter minimizations lead to same answer
//     std::cout << "running another iteration of minimization. difference is: " << (s(p)-s(before)) << std::endl;
//     before = p;
//     min_struc.run(p,mm,s,options);
//   }
  
// }


// bool
// already_minimized(std::string query,utility::vector1<std::string> check){
// 	//std::cout << "query is " << query << std::endl;
// 	for ( unsigned int i=1; i <= check.size(); i++ ) {
// 		//std::cout << "DEBUG: tag" << check[i] << std::endl;
// 		if ( check[i].compare(query) == 0 ) {
// 			//std::cout << "query " << query << " totall matches " << check[i] << std::endl;
// 			return true;
// 		}
// 	}
// 	return false;
// }

// Above code copied from minimize_with_constraints.cc

Shades::Shades()
{
}

void Shades::setup_design(EdaPose & pose, ShadesMoverOP& prot_ptr)
{
  if (option[ shades::backbone ].user())
    {
      std::string fn = option[ shades::backbone ];
      core::import_pose::pose_from_file( pose, fn, core::import_pose::PDB_file );
    }
  else
    {
      tr.Info << "Error: no input structure specified" << endl;
      exit(0);
    }
  if (!option [shades::items].user())
    {
      tr.Info << "Error: no ITEMs library specified" << endl;
      exit(0);
    }
  std::string relax_mode = option [shades::relax_mode];
  if (relax_mode.compare("minimization")!=0 && relax_mode.compare("fastrelax")!=0 )
    {
      tr.Info << "Error: Unrecognized value for shades::relax_mode. Choose between minimization and fastrelax." << endl;
      exit(0);
    }
  string items = option [shades::items];
  ShadesLoaderOP loader_ptr(new ShadesLoader(items.c_str()));
  if (option [shades::symmetry].user())
    if (loader_ptr->nb_frames() % option [shades::symmetry])
      {
        tr.Info << "Error: wrong symmetry value" << endl;
        exit(0);
      }
  utility::vector1<int> forbidden_pos;
  if (option [shades::forbidden].user())
    {
      istringstream tokenizer(option [shades::forbidden]);
      int value;
      while (tokenizer >> value)
        forbidden_pos.push_back(value);
    }
  option.add_relevant(shades::pop_threshold);
  option.add_relevant(shades::distrib_k);
  option.add_relevant(shades::nb_iterations);
  option.add_relevant(out::pdb);
  ShadesMoverOP sm_(new ShadesMover(loader_ptr, loader_ptr->nb_frames(), option [ shades::pop_threshold ] /* threshold */, option [ shades::distrib_k ], forbidden_pos));
  prot_ptr = sm_;
}

void Shades::design(EdaPose & init_pose, ShadesMoverOP prot_ptr)
{
  using protocols::jobdist::BasicJob;
  using protocols::jobdist::BasicJobOP;
  using protocols::jobdist::PlainSilentFileJobDistributor;
  using protocols::relax::FastRelax;
  int const nstruct = std::max( 1, option [ out::nstruct ]() / option [ shades::nb_iterations ] );
  utility::vector1< BasicJobOP > input_jobs;
  string seq_file;
  seq_file = option [ shades::out_sequences ];
  BasicJobOP job(new BasicJob("" /*no input tag*/, "Shades", nstruct));
  input_jobs.push_back( job );
  ShadesJobDistributor jobdist( input_jobs );
  BasicJobOP curr_job;
  int curr_nstruct;
  ShadesMover & shades( *prot_ptr);
  jobdist.startup();
  bool bEndrun = false;
  // Utiliser ShadesMover ici au lieu de abinitio_protocol
  std::string output_tag = shades.get_current_tag();
  // setup scorefunctions
  // this is called in setup_fold: add_constraints( extended_pose ); //such that scorefxn setup knows about constraints...
  core::scoring::ScoreFunctionOP centroid_scorefxn( generate_scorefxn( false /*fullatom*/ ) );
  core::scoring::ScoreFunctionOP fullatom_scorefxn( generate_scorefxn( true /*fullatom*/ ) );
  core::scoring::ScoreFunctionOP relax_scorefxn( generate_scorefxn( true /*fullatom*/ ) );
  relax_scorefxn->initialize_from_file("beta_nov15.wts");
  std::string relax_mode = option [shades::relax_mode];
  MoverOP minimize_shades( new Minimize() );
  protocols::relax::FastRelax fast_relax(relax_scorefxn);
  for (unsigned nb_iter=1; nb_iter<= (unsigned) option [ shades::nb_iterations ] ;nb_iter++) {
    // production loop
    while ( jobdist.next_job(curr_job, curr_nstruct) && !bEndrun ) {
      time_t pdb_start_time = time(NULL);
      
      EdaPose design_pose ( init_pose );
      std::ostringstream oss;
      oss << nb_iter;
      shades.set_current_tag( jobdist.get_current_output_tag()+"_"+oss.str() );
      

      // APPLY DESIGN HERE
      shades.init(design_pose);
      core::pack::task::PackerTaskOP init_task( core::pack::task::TaskFactory::create_packer_task( design_pose ));
      init_task->restrict_to_repacking();
      core::pack::pack_rotamers(design_pose,*fullatom_scorefxn,init_task);

      EdaPose mutated_pose(design_pose);
      shades.set_current_job( curr_job );
      
      if ( !design_pose.is_fullatom() ) {
        EdaPose const centroid_pose ( design_pose );
        protocols::abinitio::ResolutionSwitcher res_switch( centroid_pose, false, true, true );
        res_switch.apply( design_pose );
      }

      // Fixbb design loop
      // protocols::moves::MonteCarlo mc(design_pose, *fullatom_scorefxn, option[OptionKeys::backrub::mc_kt] );
      // mc.reset(mutated_pose);
      for(size_t i=1;i<=shades.get_nb_frames();i++)
        //      size_t ntrials_ = option[ OptionKeys::backrub::ntrials ];
        //  for(size_t i=1;i<=ntrials_;i++)
        {
          shades.apply(mutated_pose);
          protocols::backrub::BackrubProtocolOP backrub_protocol( new protocols::backrub::BackrubProtocol() );
          backrub_protocol->apply(mutated_pose);
          core::pack::task::PackerTaskOP task( core::pack::task::TaskFactory::create_packer_task( mutated_pose ));
          task->restrict_to_repacking();
          core::pack::pack_rotamers(mutated_pose,*fullatom_scorefxn,task);
          if (fullatom_scorefxn->score(design_pose)<fullatom_scorefxn->score(mutated_pose))
            mutated_pose = design_pose;
          else 
            design_pose = mutated_pose;
          // mc.boltzmann(mutated_pose);
        }
      // mc.show_counters();
      // design_pose = (EdaPose &) mc.lowest_score_pose();
      // Run Relax
      protocols::idealize::IdealizeMover idealizer;
      idealizer.fast( false );
      idealizer.apply( design_pose );

      //      fast_relax.apply(design_pose);
      if (relax_mode.compare("minimization")==0)
        minimize_shades->apply(design_pose);
      else
        fast_relax.apply(design_pose);
      //Some code here was dismissed and copied in DumpedFromShades
      // core::pack::task::PackerTaskOP task( core::pack::task::TaskFactory::create_packer_task( mutated_pose ));
      // task->restrict_to_repacking();
      // core::pack::pack_rotamers(design_pose,*relax_scorefxn,task);

      //      io::silent::SilentFileData outsfd;

      // Score
      process_decoy(design_pose, *relax_scorefxn);
      // Not working
      //  jobdist.dump_silent( outsfd);

      jobdist.store_score_and_frags(design_pose,design_pose.is_fullatom() ? *relax_scorefxn : *centroid_scorefxn);
      //      design_pose.dump_pdb(curr_job->output_tag(curr_nstruct));
      if ( option [ out::pdb ].user() )
        {
          //          cout << "Dumping pose" << endl;
          design_pose.dump_pdb(shades.get_current_tag()+".pdb");
        }
      jobdist.dump_sequence_and_score(seq_file, shades.get_current_tag(), *relax_scorefxn,design_pose);
      time_t pdb_end_time = time(NULL);
      tr.Info << "Finished " << curr_job->output_tag(curr_nstruct) << " in " << (pdb_end_time - pdb_start_time) << " seconds." << std::endl;

    } // end of production loop
    if ( option [shades::out_probas].user() )
      {
        std::ostringstream oss;
        oss << nb_iter;
        std::string fileName = option [shades::out_probas]+"_"+oss.str()+".dat";
    
        ofstream data(fileName.c_str());
        prot_ptr->print_probas(data);
      }
    jobdist.estimation_of_distribution(prot_ptr);
    //	jobdist.print_keys(tr);
    jobdist.reload( input_jobs );
  }//End of iteration X
  jobdist.shutdown();
}

void Shades::run()
{
  ShadesMoverOP prot_ptr;
  EdaPose pose;
  setup_design(pose, prot_ptr);
  design(pose, prot_ptr);
  
}

// core::scoring::ScoreFunctionOP Shades::generate_scorefxn(bool fullatom)
// {
//   core::scoring::ScoreFunctionOP scorefxn( NULL );
//   if ( fullatom ) {
//     scorefxn = core::scoring::getScoreFunction();
//   } else {
//     if ( option[  basic::options::OptionKeys::abinitio::membrane ]() ) {
//       scorefxn = core::scoring::ScoreFunctionFactory::create_score_function( "score_membrane" );
//     } else if ( option[ OptionKeys::abinitio::stage4_patch ].user() ) {
//       scorefxn = core::scoring::ScoreFunctionFactory::create_score_function( "score3", option[ OptionKeys::abinitio::stage4_patch ]() );
//     } else {
//       scorefxn = core::scoring::ScoreFunctionFactory::create_score_function( "score3" );
//     }
//   }
//   return scorefxn;
// }

core::scoring::ScoreFunctionOP Shades::generate_scorefxn( bool fullatom ) {
	core::scoring::ScoreFunctionOP scorefxn( NULL );
	if ( fullatom ) {
		scorefxn = core::scoring::get_score_function();
	} else {
		if ( option[  basic::options::OptionKeys::abinitio::membrane ]() ) {
			scorefxn = core::scoring::ScoreFunctionFactory::create_score_function( "score_membrane" );
		} else if ( option[ OptionKeys::abinitio::stage4_patch ].user() ) {
			scorefxn = core::scoring::ScoreFunctionFactory::create_score_function( "score3", option[ OptionKeys::abinitio::stage4_patch ]() );
		} else {
			scorefxn = core::scoring::ScoreFunctionFactory::create_score_function( "score3" );
		}
	}
	// if ( jump_def_  && !option[ OptionKeys::jumps::no_chainbreak_in_relax ] ) {
	// 	scorefxn->set_weight( core::scoring::linear_chainbreak, 1.0 );
	// 	scorefxn->set_weight( core::scoring::overlap_chainbreak, 1.0 );
	// }
	// if ( cstset_ && cstset_->has_residue_pair_constraints()  ) {
	// 	scorefxn->set_weight( core::scoring::atom_pair_constraint, option[ OptionKeys::constraints::cst_weight ]() );
	// 	scorefxn->set_weight( core::scoring::angle_constraint, option[ OptionKeys::constraints::cst_weight ]() );
	// 	scorefxn->set_weight( core::scoring::dihedral_constraint, option[ OptionKeys::constraints::cst_weight ]() );
	// }
	// if ( option[ OptionKeys::loopfcst::coord_cst_weight ].user() ) {
	// 	scorefxn->set_weight( core::scoring::coordinate_constraint, option[ OptionKeys::loopfcst::coord_cst_weight ]);
	// }
	return scorefxn;
}


void Shades::process_decoy(EdaPose & pose,
                           core::scoring::ScoreFunction const& scorefxn)
{
  scorefxn( pose );
  // run PoseEvaluators
  //evaluator_->apply( pose, tag, pss );
}
