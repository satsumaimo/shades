#ifndef SHADES_HH
#define SHADES_HH

// #include <utility/pointer/boost/owning_ptr.hh>
// #include <utility/pointer/boost/access_ptr.hh>
// #include <utility/pointer/boost/ReferenceCount.hh>

#include "EdaPose.hh"
#include "ShadesMover.fwd.hh"
#include <core/scoring/ScoreFunction.fwd.hh>
#include <core/io/silent/silent.fwd.hh>
#include <core/io/silent/SilentStructFactory.hh>
#include <core/io/silent/SilentFileData.fwd.hh>
#include <core/io/silent/SilentFileData.hh>
#include <protocols/evaluation/PoseEvaluator.fwd.hh>
#include <protocols/evaluation/PoseEvaluator.hh>

class Shades {
public:
  Shades();
  void setup_design(EdaPose & pose, ShadesMoverOP& prot_ptr);
  void design(EdaPose & pose, ShadesMoverOP prot_ptr);
  void run();
  // Copied from AbrelaxApplication
  core::scoring::ScoreFunctionOP generate_scorefxn(bool fullatom);
  void process_decoy(EdaPose & pose,
                     core::scoring::ScoreFunction const& scorefxn);
protected:
  // a bunch of PoseEvaluators for process_decoy() --- if available
  protocols::evaluation::MetaPoseEvaluatorOP evaluator_;
  // a score file ( written to in process_decoy )
  core::io::silent::SilentFileDataOP silent_score_file_;


};


#endif
