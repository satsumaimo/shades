#include "ShadesJobDistributor.hh"
#include "ShadesMover.hh"
#include <basic/Tracer.hh>
#include <fstream>
#include <sstream>
#include <core/scoring/ScoreFunction.hh>

static basic::Tracer tr("ShadesJobDistributor");

ShadesJobDistributor::ShadesJobDistributor(JobVector jobs):
	Parent(jobs)
{
}

void ShadesJobDistributor::dump_sequence_and_score(std::string _fileOut, std::string tag, core::scoring::ScoreFunction const& scorefxn, EdaPose & pose)
{
  #ifdef USEMPI
  if (get_mpi_rank()!=0)
    {
      std::ostringstream oss;
      oss << get_mpi_rank();
      std::string fileName = _fileOut+"_"+oss.str()+".dat";
      std::ofstream data(fileName.c_str(), std::fstream::app);
      if (not data.is_open())
        {
          std::cout << "Warning: Couldn't open file " << fileName << std::endl;
        }
      else
        {
          data << tag << " " << pose.sequence() << " " << scorefxn(pose) << std::endl;
          data.close();
        }
    }
  #endif
}


void ShadesJobDistributor::estimation_of_distribution(ShadesMoverOP& prot_ptr)
{
#ifdef USEMPI
  if (get_mpi_rank()==0)
    {
      time_t shipping_start_time = time(NULL);
      receive_scores_and_keys(prot_ptr->threshold());
      // for (Size i=0;i<nstruct;i++)
      // 	jobdist.receive_fragkeys();
      time_t shipping_end_time = time(NULL);
      tr.Info << "Shipped data to master in " << shipping_end_time - shipping_start_time << " seconds." << std::endl; 
      prot_ptr->update_probas(fragkeys_list_);
    }
  else {
    ship_scores();
    receive_order_and_send_keys();
  }
  prot_ptr->notify_probas();
  return;
#endif

}
