#ifndef SHADES_JOB_DISTRIBUTOR_HH
#define SHADES_JOB_DISTRIBUTOR_HH

// #include <utility/pointer/boost/owning_ptr.hh>
// #include <utility/pointer/boost/access_ptr.hh>
// #include <utility/pointer/boost/ReferenceCount.hh>

#include "EdaJobDistributor.hh"
#include "ShadesMover.fwd.hh"
#include <string>

class ShadesJobDistributor: public EdaJobDistributor
{
public:
  typedef EdaJobDistributor Parent;

protected:
  typedef utility::vector1< BasicJobOP > JobVector;

public:
  ShadesJobDistributor(JobVector jobs);
  void dump_sequence_and_score(std::string _fileOut, std::string tag, core::scoring::ScoreFunction const& scorefxn, EdaPose & pose);
  // Have to do this because of bad design, need to refactorize some stuff
  void estimation_of_distribution(ShadesMoverOP& prot_ptr); 
};


#endif 
