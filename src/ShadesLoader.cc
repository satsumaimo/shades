#include <iostream>
#include <fstream>
#include <sstream>
#include "ShadesLoader.hh"


using namespace std;

ShadesLoader::ShadesLoader(const char* library): seqlib(library) 
{
  load_seqlib();
}

void ShadesLoader::load_seqlib()
{
  ifstream data(seqlib);
  if (not data.is_open())
    {
      cout << "Couldn't open file " << seqlib << endl;
      exit(1);
    }
  string s;
  istringstream line;
  utility::vector1 < Fragment > residue;
  Fragment fragment;
  while(!data.eof())
    {
      getline(data,s);
      line.str(s);
      line.clear();
      if (line.str()[0]=='#')
        line.str().clear();
      else  if (line.str().empty())
        {
          line.str().clear();
        }
      else if (line.str().find("BEGIN")==0)
        {
          // We start a new residue record
          size_t resnum;
          char flsh[5];
          // Flushing keyword
          line >> flsh;
          line >> resnum;
          fragment.resnum = resnum;
        }
      else if (line.str().find("END")==0)
        {
          // We end a residue record
          frag_lib.push_back(residue);
          residue.clear();
        }
      else
        {
          // We parse a fragment description:
          // Nb of contacts, Resid, list of Resid+Resnum (contacts)
          size_t nb_contacts;
          size_t resnum;
          char resid;
          line >> nb_contacts;
          line >> resid;
          fragment.resid = resid;
          for (unsigned i = 0; i<nb_contacts; i++)
            {
              line >> resid;
              line >> resnum;
              Contacts cur_contact(resnum, resid);
              fragment.contacts.push_back(cur_contact);
            }
          residue.push_back(fragment);
          fragment.contacts.clear();
        }
    }
}

void ShadesLoader::show_lib()
{
  for (unsigned i=1;i<=frag_lib.size();i++)
    {
      for (unsigned j=1;j<=frag_lib[i].size();j++)
        {
          cout << "Residue :" << frag_lib[i][j].resnum << ":" << frag_lib[i][j].resid << endl;
          cout << "Contacts:" << endl;
          for (unsigned k=1; k<=frag_lib[i][j].contacts.size();k++)
            cout << frag_lib[i][j].contacts[k].resnum << " " << frag_lib[i][j].contacts[k].resid << endl;
        }
    }
}

ShadesLoader::Fragment & ShadesLoader::get_fragment(EdaPose::Item item)
{
  return frag_lib[item.frame_pos][item.frag_num];
}

size_t ShadesLoader::nb_frames()
{
  return frag_lib.size();
}

size_t ShadesLoader::nb_frags()
{
  if (frag_lib.size())
    return frag_lib[1].size();
  else
    return 0;
}

size_t ShadesLoader::nb_frags(size_t frame)
{
  if (frag_lib.size()>=frame)
    return frag_lib[frame].size();
  else
    return 0;
}
