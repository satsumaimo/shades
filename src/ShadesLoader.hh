// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
// vi: set ts=2 noet:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @file ShadesLoader.hh
/// @brief header file for Loading sequence library
/// @detailed
///	  Contains currently: ShadesLoader
///
///
/// @author David Simoncini

#ifndef Shades_loader_hh
#define Shades_loader_hh

#include "EdaPose.hh"
#include "ShadesLoader.fwd.hh"
#include <core/types.hh>
#include <utility/vector1.fwd.hh>
#include <utility/vector1.hh>
#include <utility/pointer/ReferenceCount.hh>

using namespace core;

class ShadesLoader: public utility::pointer::ReferenceCount {
public:
  struct Contacts {
		Contacts(size_t num, const char id) : resnum(num), resid(id) {}
		Contacts() : resnum(0), resid('A') {}
    size_t resnum;
    char resid;
  };
  typedef struct Fragment {
    Fragment(size_t num, const char id, utility::vector1< Contacts > _contacts) : resnum(num), resid(id), contacts(_contacts) {}
    Fragment() : resnum(0), resid('A') {}
		size_t resnum;
    char resid;
    utility::vector1< Contacts > contacts;
  } Fragment;

  ShadesLoader(const char* library);
  void load_seqlib();
	void show_lib();
	Fragment & get_fragment(EdaPose::Item item);
	size_t nb_frames();
	size_t nb_frags();
	size_t nb_frags(size_t frame);

private:
  const char* seqlib;
	utility::vector1 < utility::vector1 < Fragment > > frag_lib;
};

#endif
