#include "ShadesMover.hh"
#include<protocols/simple_moves/MutateResidue.hh> 
#include <numeric/random/random.hh>
#include <basic/options/option.hh>
#include <basic/options/keys/in.OptionKeys.gen.hh>
#include <basic/options/keys/out.OptionKeys.gen.hh>
#include <basic/options/keys/abinitio.OptionKeys.gen.hh>
#include <basic/options/keys/shades.OptionKeys.gen.hh>

#ifdef USEMPI
#include <mpi.h>
#endif


//static numeric::random::RandomGenerator RG(492);

using namespace std;
using namespace basic::options;
using namespace basic::options::OptionKeys;


ShadesMover::ShadesMover(ShadesLoaderOP _loader, size_t _nb_frames, double _threshold, double dist_k, utility::vector1<unsigned> _forbidden_pos):loader(_loader), 
                                                                                                                                            //backrubmover(protocols::backrub::BackrubMoverOP( new protocols::backrub::BackrubMover() )),
                                                                                                                                            nb_frames(_nb_frames), 
                                                                                                                                            threshold_(_threshold), k(dist_k), forbidden_pos(_forbidden_pos)
{
  init_frag_keys();
  if (option [ shades::symmetry ].user())
    symmetry= option [ shades::symmetry ];
  else
    symmetry=0;
}

ShadesMover::ShadesMover(ShadesMover const & src):Parent(src),
                                                  loader(src.loader),
                                                  fragKeys(src.fragKeys), 
                                                  nb_frames(src.nb_frames), 
                                                  threshold_(src.threshold_),
                                                  symmetry(src.symmetry),
                                                  forbidden_pos(src.forbidden_pos)
{
}

// Insert fragments in each frame randomly to start the search from a random point
void ShadesMover::init(core::pose::Pose & pose)
{
  utility::vector1< size_t > rankTable; 
  for(size_t i=1;i<=nb_frames;i++)
    rankTable.push_back(i);
  for(size_t i=1;i<=nb_frames;i++)
    {
  size_t size = rankTable.size();
  size_t frame_index = static_cast< size_t > (numeric::random::rg().uniform() * size  + 1);
  size_t frag_index = roulette_wheel(fragKeys[rankTable[frame_index]]);
  ((EdaPose &)pose).set_cur_item(EdaPose::Item(rankTable[frame_index], frag_index));
  rankTable[frame_index] = rankTable[size];
  rankTable.pop_back();
  mutate(pose);
  //((EdaPose &)pose).pushback_frags(((EdaPose &)pose).get_cur_item());
    }

}


void ShadesMover::apply(core::pose::Pose & pose)
{
  utility::vector1< size_t > rankTable; 
  for(size_t i=1;i<=nb_frames;i++)
    rankTable.push_back(i);
  //  for(size_t i=1;i<=nb_frames;i++)
  //  {
  size_t size = rankTable.size();
  size_t frame_index = static_cast< size_t > (numeric::random::rg().uniform() * size  + 1);
  size_t frag_index = roulette_wheel(fragKeys[rankTable[frame_index]]);
  ((EdaPose &)pose).set_cur_item(EdaPose::Item(rankTable[frame_index], frag_index));
  rankTable[frame_index] = rankTable[size];
  rankTable.pop_back();
  //  backrubmover->clear_segments();
  mutate(pose);
  //  backrubmover->set_input_pose(core::pose::PoseOP(new core::pose::Pose(pose)));
  // backrubmover->add_mainchain_segments();
  // if (backrubmover->num_segments())
  //   backrubmover->apply(pose);
  ((EdaPose &)pose).pushback_frags(((EdaPose &)pose).get_cur_item());
  //  }

}

void ShadesMover::mutate(core::pose::Pose & pose)
{
  ShadesLoader::Fragment frag = loader->get_fragment(((EdaPose &)pose).get_cur_item());
  // utility::vector1<size_t> pivot_residues;
  if (!is_forbidden(frag.resnum))
    {
      protocols::simple_moves::MutateResidue mutated(frag.resnum, frag.resid);
      //   pivot_residues.push_back(frag.resnum);
      mutated.apply(pose);    
      if (symmetry)
        handle_symmetry(pose, frag.resnum, frag.resid);
    }
  for (size_t i=1; i<=frag.contacts.size(); i++)
    {
      if (!is_forbidden(frag.contacts[i].resnum))
        {
          protocols::simple_moves::MutateResidue mutated(frag.contacts[i].resnum, frag.contacts[i].resid);
          //  pivot_residues.push_back(frag.contacts[i].resnum);
          mutated.apply(pose);
          if (symmetry)
            handle_symmetry(pose, frag.contacts[i].resnum, frag.contacts[i].resid);
        }
    }
  //  backrubmover->set_pivot_residues(pivot_residues);
  pose.update_residue_neighbors();
}

void ShadesMover::handle_symmetry(core::pose::Pose & pose, size_t resnum, char resid)
{
  size_t sym_res;
  size_t cur_res=resnum;
  while ((sym_res=(cur_res+symmetry)%nb_frames)!=(resnum%nb_frames))
    {
      if (sym_res==0)
        sym_res=nb_frames;
      protocols::simple_moves::MutateResidue mutated(sym_res, resid);
      mutated.apply(pose);
      cur_res = sym_res;
    }
}

bool ShadesMover::is_forbidden(size_t resnum)
{
  for(size_t i=1;i<=forbidden_pos.size();i++)
    if (forbidden_pos[i]==resnum)
      return true;
  return false;
}

void ShadesMover::init_frag_keys() {
  for ( size_t i=1; i<=nb_frames; i++ )
    {
      utility::vector1<double> tmp_vec;
      size_t nb_frags = loader->nb_frags(i);
      //cout << "Loaded " << loader->nb_frags(i) << " for frame " << i << endl;
      double p = (double) 1/nb_frags;      
      for (size_t i=1; i<=nb_frags; i++)
        tmp_vec.push_back(p);
      fragKeys.push_back(tmp_vec);
    }
}

void ShadesMover::update_probas(utility::vector1< utility::vector1< EdaPose::Item > > & fragkeys_list) {
  double obs;
  if (fragKeys.size()==0) {
    std::cout << "Empty probas..." << std::endl;
    return;
  }
  utility::vector1< utility::vector1<Size> > fragCounts(fragKeys.size());
  utility::vector1<Size> fragTotals(fragKeys.size(),0);
  for (unsigned i=1;i<=fragKeys.size();i++)
    fragCounts[i].resize(fragKeys[i].size(),0);
  for (unsigned i=1;i<=fragkeys_list.size();i++)
    for (unsigned j=1;j<=fragkeys_list[i].size();j++) {
      fragCounts[fragkeys_list[i][j].frame_pos][fragkeys_list[i][j].frag_num]++;
      fragTotals[fragkeys_list[i][j].frame_pos]++;
    }
  for (unsigned i=1;i<=fragKeys.size();i++)
    if (fragTotals[i]) {
      for (unsigned j=1;j<=fragKeys[i].size();j++) {
        obs = (double) (fragCounts[i][j]) / (double) (fragTotals[i]);
        fragKeys[i][j]=k*fragKeys[i][j]+(1-k)*obs;
      }
    }
}

void ShadesMover::load_probas(utility::vector1< utility::vector1<double> > newFragKeys) {
  fragKeys=newFragKeys;
}


utility::vector1< utility::vector1<double> > & ShadesMover::get_frag_keys() {
  return fragKeys;
}

void ShadesMover::print_probas(std::ostream & output) {
  if (fragKeys.size()==0) {
    output << "Empty probas..." << std::endl;
  }
  for (unsigned i=1;i<=fragKeys.size();i++) {
    for (unsigned j=1;j<=fragKeys[i].size();j++) {
      output << fragKeys[i][j] << " ";
    }
    output << std::endl;
  }
}


size_t ShadesMover::roulette_wheel(utility::vector1<double> vec, double total) const {
  if (!vec.size())
    {
      cout << "[roulette_wheel] WARNING: NO VECTOR IN INPUT" << endl;
      return 0;
    }
  if (total == 0)
    { // count
      for (unsigned    i = 1; i <= vec.size(); i++)
        total += vec[i];
    }
  double fortune = numeric::random::rg().uniform()*total;
  Size i=0;
  for (i=1;i<vec.size();i++)
    {
      fortune -= vec[i];
      if (fortune <= 0)
        break;
    }
  return i;
}

void ShadesMover::notify_probas()
{
  int myrank;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  utility::vector1< utility::vector1<double> > newKeys(fragKeys.size());
  for (unsigned i=1;i<=newKeys.size();i++)
    newKeys[i].resize(fragKeys[i].size(), (double) 0);
  if (myrank==0)
    newKeys=fragKeys;
  //MPI_Type_vector(1, newKeys.size(), newKeys.front().size(),MPI_DOUBLE, &all_vecs);

  for (unsigned i=1;i<=newKeys.size();i++)
    {
      MPI_Datatype all_vecs;
      MPI_Type_contiguous(newKeys[i].size(), MPI_DOUBLE, &all_vecs);
      MPI_Type_commit(&all_vecs);
      MPI_Bcast(&newKeys[i].front(),1,all_vecs,0,MPI_COMM_WORLD);
      MPI_Type_free(&all_vecs);
    }
  if (myrank!=0)
    {
      load_probas(newKeys);
    }

}

double ShadesMover::threshold()
{
  return threshold_;
}

std::string ShadesMover::get_name() const
{
  return "ShadesMover";
}
