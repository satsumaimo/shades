// -*- mode:c++;tab-width:2;indent-tabs-mode:t;show-trailing-whitespace:t;rm-trailing-spaces:t -*-
// vi: set ts=2 noet:
//
// (c) Copyright Rosetta Commons Member Institutions.
// (c) This file is part of the Rosetta software suite and is made available under license.
// (c) The Rosetta software is developed by the contributing members of the Rosetta Commons.
// (c) For more information, see http://www.rosettacommons.org. Questions about this can be
// (c) addressed to University of Washington UW TechTransfer, email: license@u.washington.edu.

/// @author David Simoncini


#ifndef SHADES_MOVER_fwd_hh
#define SHADES_MOVER_fwd_hh


// Utility headers
// #include <utility/pointer/boost/owning_ptr.hh>
// #include <utility/pointer/boost/access_ptr.hh>
// #include <utility/pointer/boost/ReferenceCount.hh>

// Forward
class ShadesMover;

// Types
typedef  utility::pointer::shared_ptr< ShadesMover >  ShadesMoverOP;
typedef  utility::pointer::shared_ptr< ShadesMover const >  ShadesMoverCOP;



#endif
