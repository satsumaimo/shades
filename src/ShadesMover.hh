#ifndef SHADES_MOVER_HH
#define SHADES_MOVER_HH

// #include <utility/pointer/boost/owning_ptr.hh>
// #include <utility/pointer/boost/access_ptr.hh>
// #include <utility/pointer/boost/ReferenceCount.hh>

#include "EdaPose.hh"
#include "ShadesMover.fwd.hh"
#include "ShadesLoader.fwd.hh"
#include "ShadesLoader.hh"
#include <utility/vector1.fwd.hh>
#include <utility/vector1.hh>
#include <protocols/moves/Mover.hh>
//#include <protocols/backrub/BackrubMover.hh>

class ShadesMover: public protocols::moves::Mover {
public:

  typedef protocols::moves::Mover Parent;

  ShadesMover(ShadesLoaderOP loader, size_t _nb_frames, double _threshold, double dist_k, utility::vector1<unsigned> _forbidden_pos);

  ShadesMover(ShadesMover const & src);

  void init( core::pose::Pose & pose );

  void apply( core::pose::Pose & pose );
			
  void mutate( core::pose::Pose & pose );

  void handle_symmetry(core::pose::Pose & pose, size_t resnum, char resid);

  bool is_forbidden(size_t resnum);

  void init_frag_keys();
			
  void update_probas(utility::vector1< utility::vector1< EdaPose::Item > > & fragkeys_list);

  void load_probas(utility::vector1< utility::vector1<double> > newFragKeys);

  void print_probas(std::ostream & output);

  size_t get_nb_frames() {return nb_frames;}

  utility::vector1< utility::vector1<double> > & get_frag_keys();

  size_t roulette_wheel(utility::vector1<double> vec, double total=0) const;
  
  void notify_probas();

  double threshold();

  std::string get_name() const;

private:
  ShadesLoaderOP loader;
  //  protocols::backrub::BackrubMoverOP backrubmover;
  utility::vector1< utility::vector1<double> > fragKeys; 
  size_t nb_frames;
  double threshold_;
  double k;
  size_t symmetry;
  utility::vector1<unsigned> forbidden_pos;
};






#endif
