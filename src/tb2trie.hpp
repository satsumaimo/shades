#ifndef TB2TRIE_HPP_
#define TB2TRIE_HPP_
#include <fstream>
using namespace std;

class TrieCpd
{
public:
    TrieCpd();
    ~TrieCpd();
    int aa2int(char aa);
    char int2aa(int i);
    bool present(char aa);
    void insert(char aa);
    void insert_sequence(string seq);
    void print_tree();
    void print_tree(ofstream & os);
    void print_tree(string acc);
  void print_tree(string acc, ofstream & os);
    size_t getTotalSequences() {return total_sequences;}
private:
    vector<TrieCpd *> sons;
    size_t sequence_count;
    static size_t total_sequences;
};

#endif
