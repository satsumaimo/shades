#ifndef TRIE_HPP_
#define TRIE_HPP_

#include "tb2trie.hpp"

using namespace std;

class Trie
{
public:
    Trie();
    ~Trie();
  int aa2int(char aa);
  char int2aa(int i);
    bool present(char i);
    void insert(char i);
  void insert_sequence(string tab, string seq);
    void print_tree();
    void print_tree(string acc);
    size_t getTotal() {return total;}
private:
    vector<Trie *> sons;
  TrieCpd * sequences;
    size_t count;
    static size_t total;
};

#endif
